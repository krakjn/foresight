/**
 * Circle M Contractors
 */
module circlem {
    requires java.desktop;
    requires javafx.controls;
    requires javafx.fxml;
    requires poi;
    requires poi.ooxml;
    requires poi.ooxml.schemas;  // for CTProperty, wow what a workaround
    requires org.json;
    requires java.logging;

    opens sched to javafx.fxml;
    exports sched;
}