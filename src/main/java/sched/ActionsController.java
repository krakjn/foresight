package sched;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.time.LocalDate;
import java.util.Optional;

import static sched.ForemenController.getCurrentForeman;
import static sched.PhasesController.getCurrentPhase;
import static sched.PhasesController.getCurrentFQN;

public class ActionsController {

    @FXML private ComboBox<String> actionComboBox;
    @FXML private ListView<Action> actionListView;
    @FXML private TextField        actionActioneer;
    @FXML private TextField        actionLumberDriver;
    @FXML private DatePicker       actionDatePicker;
    @FXML private TextArea         actionNotes;

    private static SimpleIntegerProperty idx;
    private static boolean actionSelected;

    // for phase to listen
    public static SimpleObjectProperty<LocalDate>  lumberDate;
    public static SimpleObjectProperty<LocalDate>  completeDate;

    final PseudoClass errorClass = PseudoClass.getPseudoClass("error");
    final PseudoClass noDateClass = PseudoClass.getPseudoClass("nodate");
    final PseudoClass noDateUnselectClass = PseudoClass.getPseudoClass("nodateUnselect");
    final PseudoClass goodClass = PseudoClass.getPseudoClass("good");
    final PseudoClass goodUnselectClass = PseudoClass.getPseudoClass("goodUnselect");
    final String COMBO_BOX_DEFAULT = "Select Action to Add";

    /**
     * job complete to first action is hatched
     * thin border all the cells
     * clear out actions based on foreman selection
     */

    public void initialize() {
        actionComboBox.getItems().addAll("TRENCH", "SLABS", "LUMBER", "SNAP", "LAYOUT", "ROOFSHEET", "JOB COMPLETE", "Select Action to Add");
        actionComboBox.setVisibleRowCount(7); // select Action to Add is on the end so it shouldn't be affected
        actionComboBox.setValue(COMBO_BOX_DEFAULT);
        ActionsController.idx = new SimpleIntegerProperty();
        ActionsController.idx.set(-1);

        actionListView.setCellFactory(lv -> new ListCell<>() {
            @Override
            protected void updateItem(Action action, boolean empty) {
                super.updateItem(action, empty);
                if (empty) {
                    setText(null);
                    pseudoClassStateChanged(noDateClass, false);
                    pseudoClassStateChanged(noDateUnselectClass, false);
                    pseudoClassStateChanged(goodClass, false);
                    pseudoClassStateChanged(goodUnselectClass, false);
                } else {
                    setText(action.getViewName());
                    if (isSelected()) {
                        if (action.isDateSet()) {
                            pseudoClassStateChanged(noDateClass, false);
                            pseudoClassStateChanged(noDateUnselectClass, false);
                            pseudoClassStateChanged(goodUnselectClass, false);
                            pseudoClassStateChanged(goodClass, true);
                        } else {
                            pseudoClassStateChanged(goodClass, false);
                            pseudoClassStateChanged(goodUnselectClass, false);
                            pseudoClassStateChanged(noDateUnselectClass, false);
                            pseudoClassStateChanged(noDateClass, true);
                        }
                    } else { // not selected
                        if (action.isDateSet()) {
                            pseudoClassStateChanged(goodClass, false);
                            pseudoClassStateChanged(noDateClass, false);
                            pseudoClassStateChanged(noDateUnselectClass, false);
                            pseudoClassStateChanged(goodUnselectClass, true);
                        } else {
                            pseudoClassStateChanged(goodClass, false);
                            pseudoClassStateChanged(noDateClass, false);
                            pseudoClassStateChanged(goodUnselectClass, false);
                            pseudoClassStateChanged(noDateUnselectClass, true);
                        }
                    }
                }
            }
        });

        actionLumberDriver.setEditable(false);
        Action.initStyles();

        PhasesController.idx.addListener((obsv, oldNumber, newNumber) -> refreshView());
        PhasesController.zeroPhase.addListener((obsv, before, after) -> refreshView());
        PhasesController.zeroIdx.addListener((obsv, before, after) -> refreshView());

        actionListView.getSelectionModel().selectedItemProperty().addListener(
            (observableValue, s, t1) -> {
                ActionsController.idx.set(actionListView.getSelectionModel().getSelectedIndex());
                Optional<Action> optionalAction = getCurrentAction();
                if (optionalAction.isPresent()) {
                    Action action = optionalAction.get();
                    if (action.type == Action.Type.LUMBER) {
                        actionActioneer.setText("");
                        actionActioneer.setPromptText("Not Used, See Below");
                    } else if (action.type == Action.Type.JOB_COMPLETE) {
                        actionActioneer.setText("");
                        actionActioneer.setPromptText("Yay! Finished");
                    } else {
                        if (action.actioneer.equals("")) {
                            actionActioneer.setPromptText("Enter Name");
                        }
                        actionActioneer.setText(action.actioneer);
                    }
                    actionLumberDriver.setText(action.lumberDriver);
                    actionLumberDriver.pseudoClassStateChanged(errorClass, false);
                    actionSelected = true; // to prevent checking validity of day, when just updating the view
                    actionDatePicker.setValue(action.getDay());
                    actionNotes.setText(action.notes);
                    //? does the list need to be refreshed?
                } else {
                    actionLumberDriver.setText("");
                    actionLumberDriver.pseudoClassStateChanged(errorClass, false);
                    actionDatePicker.setValue(null);
                    actionNotes.setText("");
                }
            }
        );

        actionDatePicker.valueProperty().addListener((observableValue, oldDay, newDay) -> {
            if (actionSelected) {
                System.out.println("action selected...");
                getCurrentAction().ifPresent(action -> action.day = newDay);
                actionSelected = false;
            } else {
                if (newDay == null) return; // fresh phase
                if (oldDay != null && oldDay.isEqual(newDay)) {
                    refreshAction(newDay);
                } else {
                    setNewAction(oldDay, newDay);
                }
            }
            actionListView.refresh();
        });
    }

    public static int getIndex() {
        return ActionsController.idx.get();
    }

    public static Optional<Action> getCurrentAction() {

        Optional<Phase> optionalPhase = getCurrentPhase();
        if (optionalPhase.isPresent()) {
            Phase phase = optionalPhase.get();
            int a = ActionsController.getIndex();
            if (phase.actions.size() > 0 && a >=0) {
                return Optional.of(phase.actions.get(a));
            }
        }
        return Optional.empty();

    }


    private void refreshView() {
        ObservableList<Action> actionList = actionListView.getItems();
        actionList.clear(); // clear out existing entries
        getCurrentPhase().ifPresent( phase -> {
            // if present repopulate
            actionList.addAll(phase.actions);
        });
        actionListView.refresh(); // needed?
        actionComboBox.getSelectionModel().clearSelection();
        actionComboBox.setValue(COMBO_BOX_DEFAULT);
        actionLumberDriver.setText("");
        actionLumberDriver.pseudoClassStateChanged(errorClass, false);
        actionActioneer.setPromptText("Select Action");
        actionActioneer.setText("");
        actionDatePicker.setValue(null);
        actionNotes.setText("");
    }


    @FXML
    private void add() {
        getCurrentPhase().ifPresent(phase -> {
            int idx = actionComboBox.getSelectionModel().getSelectedIndex();
            if (idx >= 0) {
                Action.Type type;
                String lumberDriver = "";
                switch (actionComboBox.getValue()) {
                    case "TRENCH" -> type = Action.Type.TRENCH;
                    case "SLABS" -> type = Action.Type.SLABS;
                    case "LUMBER" -> {
                        type = Action.Type.LUMBER;
                        Optional<Driver> optionalDriver = DriversController.getCurrentDriver();
                        if (optionalDriver.isPresent()) {
                            actionActioneer.setText(""); // clear out actioneer
                            lumberDriver = optionalDriver.get().name;
                            actionLumberDriver.setText(lumberDriver);
                            actionLumberDriver.pseudoClassStateChanged(errorClass, false);
                        } else {
                            actionLumberDriver.setText(null);
                            actionLumberDriver.setPromptText("No Driver Selected");
                            actionLumberDriver.pseudoClassStateChanged(errorClass, true);
                            return; // don't add to view
                        }
                    }
                    case "SNAP" -> type = Action.Type.SNAP;
                    case "LAYOUT" -> type = Action.Type.LAYOUT;
                    case "ROOFSHEET" -> type = Action.Type.ROOFSHEET;
                    case "JOB COMPLETE" -> {
                        // enum.toString retains "_" , combobox doesn't have the "_"
                        for (Action action: phase.actions) {
                            if (action.type == Action.Type.JOB_COMPLETE) {
                                return; // already added
                            }
                        }
                        type = Action.Type.JOB_COMPLETE;
                    }

                    default -> { return; }
                }
                Action newAction = new Action(type, phase.getNumOfActions(type), lumberDriver);
                newAction.fqn = getCurrentFQN();
                actionListView.getItems().add(newAction);
                phase.actions.add(newAction);
                actionListView.getSelectionModel().selectLast();
                actionSelected = false;
            }
        });
    }

    @FXML
    private void remove() {
        getCurrentPhase().ifPresent(phase -> {
            int idx = actionListView.getSelectionModel().getSelectedIndex();
            if (idx >= 0) {
                // update Excel viewMap
                getCurrentForeman().ifPresent(foreman -> {
                    Action deleteAction = phase.actions.get(idx);
                    foreman.viewMap.remove(deleteAction.day);
                });

                ObservableList<Action> actionList = actionListView.getItems();
                Action removedAction = actionList.get(idx);
                phase.actions.remove(idx);  // update model
                actionList.remove(idx);     // update view

                switch (removedAction.type) {
                    case TRENCH: phase.reorder(Action.Type.TRENCH);
                    case SLABS: phase.reorder(Action.Type.SLABS);
                    case LUMBER: {
                        phase.reorder(Action.Type.LUMBER);
                        LocalDate earliestLumberDay = phase.getEarliestLumberDay(null);
                        phase.tf.setStart(earliestLumberDay); // can be null!
                        lumberDate.set(earliestLumberDay);    // can be null!
                    }
                    case SNAP: phase.reorder(Action.Type.SNAP);
                    case LAYOUT: phase.reorder(Action.Type.LAYOUT);
                    case ROOFSHEET: phase.reorder(Action.Type.ROOFSHEET);
                    case JOB_COMPLETE: {
                        // JOB COMPLETE doesn't need reorder
                        phase.tf.setEnd(null);
                        completeDate.set(null);
                    }
                }
            }
        });
    }

    private void setNewAction(LocalDate oldDay, LocalDate newDay) {
        // set Action, make sure no action is already on "this" date

        Optional<Foreman> optionalForeman = getCurrentForeman();
        optionalForeman.ifPresent(foreman -> {


            /// Need to check for other jobs loaded on this day
            for (Job job: foreman.jobs) {
                for (Phase phase: job.phases) {
                    for (Action action: phase.actions) {

                        if (action.day != null) {
                            // the last action will have a null day (because we are about to add it)
                            if (action.day.isEqual(newDay)) {
                                Foresight.warnUserDialog("Date reserved",
                                        "Another action is currently allocated for " + newDay.toString() + "\n" +
                                                "Job: " + job.name + "\n" +
                                                "Phase: " + phase.name + "\n" +
                                                "Action: " + action.type.toViewString() + " ");
                                actionDatePicker.setValue(null); // clear our entry
                                return;
                            }

                        }

                    }
                }
            }

            // then check the action against this phase's JOB_COMPLETE
            getCurrentPhase().ifPresent(phase -> {
                for (Action action : phase.actions) {
                    if (action.day != null && action.type == Action.Type.JOB_COMPLETE) {
                        if (newDay.isAfter(action.day)) {
                            // Lumber Action is after JOB_COMPLETE
                            Foresight.warnUserDialog("Lumber After Job Complete",
                                    "Lumber Action won't appear on schedule if after JOB_COMPLETE on the same phase");
                            actionDatePicker.setValue(null); // clear our entry
                            return;
                        }
                    }
                }
            });

            // All clear, add the Action
            getCurrentAction().ifPresent(action -> {
                action.day = newDay;  // update model
                getCurrentPhase().ifPresent(phase -> {
                    if (action.type == Action.Type.LUMBER) {
                        LocalDate lumberDay = phase.getEarliestLumberDay(newDay);
                        phase.tf.setStart(lumberDay);
                        lumberDate.set(lumberDay);
                    } else if (action.type == Action.Type.JOB_COMPLETE) {
                        phase.tf.setEnd(action.day);
                        completeDate.set(action.day);
                    }
                });
                foreman.viewMap.remove(oldDay);
                foreman.viewMap.put(action.day, action);
            });
        });
    }

    private void refreshAction(LocalDate day) {
        Optional<Action> optionalAction = getCurrentAction();
        optionalAction.ifPresent(action -> {
            action.day = day; // update model
            getCurrentPhase().ifPresent(phase -> {
                if (action.type == Action.Type.LUMBER) {
                    LocalDate lumberDay = phase.getEarliestLumberDay(day);
                    phase.tf.setStart(lumberDay);
                    lumberDate.set(lumberDay);
                } else if (action.type == Action.Type.JOB_COMPLETE) {
                    phase.tf.setEnd(day);
                    completeDate.set(day);
                }
            });
            getCurrentForeman().ifPresent(foreman -> {
                foreman.viewMap.remove(day);
                foreman.viewMap.put(action.day, action);
            });

        });
    }

    @FXML
    private void setActioneer() {
        Optional<Action> optionalAction = ActionsController.getCurrentAction();
        if (optionalAction.isPresent()) {
            optionalAction.get().actioneer = actionActioneer.getText();
        } else {
            actionActioneer.setText("");
        }
    }

    @FXML
    private void setNotes() {
        Optional<Action> optionalAction = getCurrentAction();
        if (optionalAction.isPresent()) {
            optionalAction.get().notes = actionNotes.getText();
        } else {
            actionNotes.setText("");
        }
    }
}
