package sched;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;

import java.util.Optional;

import static sched.PhasesController.getCurrentFQN;

public class JobsController {
    @FXML ListView<String>  jobsListView;
    @FXML TextField         developerTextField;
    @FXML TextField         superintendentTextField;
    @FXML TextField         lumberTextField;
    @FXML TextArea          notesTextArea;

    public static SimpleIntegerProperty idx;
    public static SimpleIntegerProperty zeroJob;
    public static SimpleIntegerProperty zeroIdx;

    /**
     * Any Controller with this method will get called at load time of FXML file
     */
    public void initialize() {
        JobsController.idx = new SimpleIntegerProperty();
        JobsController.idx.set(-1);
        JobsController.zeroJob = new SimpleIntegerProperty();
        JobsController.zeroJob.set(0);
        JobsController.zeroIdx = new SimpleIntegerProperty();
        JobsController.zeroIdx.set(0);
        jobsListView.setCellFactory(TextFieldListCell.forListView());
        jobsListView.setEditable(true);  // false by default

        ForemenController.idx.addListener((obsv, before, after) -> refreshView());
        ForemenController.zeroForeman.addListener((obsv, before, after) -> refreshView());
        ForemenController.zeroIdx.addListener((obsv, before, after) -> refreshView());
        zeroJob.addListener((obsv, before, after) -> refreshView());

        // This is called after and edit of the list cell
        // Also happens upon selection
        jobsListView.getSelectionModel().selectedItemProperty().addListener(
            (observableValue, oldName, newName) -> {
                jobsListView.layout();  // update UI
                int idx = jobsListView.getSelectionModel().getSelectedIndex();
                if (idx < 0) { // edits come in as -1
                    jobsListView.getSelectionModel().select(jobsListView.getEditingIndex());
                    // selection causes re-entry
                    return;
                }
                JobsController.idx.set(idx);
                getCurrentJob().ifPresent((job) -> {
                    // update view
                    job.name = newName;
                    developerTextField.setText(job.developer);
                    superintendentTextField.setText(job.superintendent);
                    lumberTextField.setText(job.lumber);
                    notesTextArea.setText(job.notes);
                    // update all jobs actions fqns
                    for (Phase phase: job.phases) {
                        for (Action action: phase.actions) {
                            action.fqn = getCurrentFQN();
                        }
                    }
                });
            }
        );

        jobsListView.setOnEditCommit(event -> {
            ObservableList<String> jobsList = jobsListView.getItems();
            int editIdx = jobsListView.getEditingIndex();
            jobsList.set(editIdx, event.getNewValue());
            jobsListView.getSelectionModel().select(editIdx);
        });

        Parser.loaded.addListener((observableValue, oldValue, newValue) -> {
            ObservableList<String> list = jobsListView.getItems();
            list.clear();
            Optional<Foreman> optionalForeman = ForemenController.getCurrentForeman();
            optionalForeman.ifPresent(foreman -> {
                for (Job job: foreman.jobs) {
                    list.add(job.name);
                }
                // cascading selection from listeners
                jobsListView.getSelectionModel().selectFirst();
            });
        });

    }

    public static int getIndex() {
        return JobsController.idx.get();
    }

    public static Optional<Job> getCurrentJob() {
        Optional<Foreman> optionalForeman = ForemenController.getCurrentForeman();
        if (optionalForeman.isPresent()) {
            Foreman foreman = optionalForeman.get();
            int j = JobsController.getIndex();
            if (foreman.jobs.size() > 0 && j >= 0) {
                return Optional.of(foreman.jobs.get(j));
            }
        }
        return Optional.empty();
    }


    private void refreshView() {
        JobsController.idx.set(-1);  // invalidate selection
        ObservableList<String> jobList = jobsListView.getItems();
        jobList.clear(); // wipe out previous jobs
        ForemenController.getCurrentForeman().ifPresent(foreman -> {
            for (Job job : foreman.jobs) {
                jobList.add(job.name);
            }
        });
        // clear fields
        developerTextField.setText("");
        superintendentTextField.setText("");
        lumberTextField.setText("");
        notesTextArea.setText("");
    }

    @FXML private void add() {
        int idx = ForemenController.getIndex();
        if (idx >= 0) {
            Foreman foreman = Foresight.foremen.get(idx);
            ObservableList<String> jobsList = jobsListView.getItems();
            jobsList.add("New Job " + foreman.jobNum);
            foreman.jobs.add(new Job(foreman.jobNum++));
            int newIdx = jobsList.size() - 1;
            jobsListView.getSelectionModel().select(newIdx);
            jobsListView.layout(); // needed to redraw immediately
            jobsListView.edit(newIdx);
        }
    }

    @FXML private void remove() {
        int f = ForemenController.getIndex();
        int j = JobsController.getIndex();
        if (f >= 0 && j >= 0) {
            if (Foresight.foremen.get(f).jobs.size() > 0) {
                Foresight.foremen.get(f).jobs.remove(j);
                jobsListView.getItems().remove(j);
                if (jobsListView.getItems().size() == 0) {
                    // for either no jobs loaded OR the zero index job removed
                    // doesn't update the index, so need to update the value manually
                    zeroJob.set(zeroJob.get() + 1);
                } else if (j == 0) {
                    zeroIdx.set(zeroIdx.get() + 1); // to help others update
                }
            }
        }
    }

    @FXML private void edit() {
        int idx = JobsController.getIndex();
        if ( idx >= 0) {
            jobsListView.edit(idx);
        }
    }

    @FXML private void setDeveloper() {
        Optional<Job> optionalJob = getCurrentJob();
        if (optionalJob.isPresent()) {
            optionalJob.get().developer = developerTextField.getText();
        } else {
            developerTextField.setText("");
        }
    }

    @FXML private void setSuperintendent() {
        Optional<Job> optionalJob = getCurrentJob();
        if (optionalJob.isPresent()) {
            optionalJob.get().superintendent = superintendentTextField.getText();
        } else {
            superintendentTextField.setText("");
        }
    }

    @FXML private void setLumber() {
        Optional<Job> optionalJob = getCurrentJob();
        if (optionalJob.isPresent()) {
            optionalJob.get().lumber = lumberTextField.getText();
        } else {
            lumberTextField.setText("");
        }
    }

    @FXML
    private void setNotes() {
        Optional<Job> optionalJob = getCurrentJob();
        if (optionalJob.isPresent()) {
            optionalJob.get().notes = notesTextArea.getText();
        } else {
            notesTextArea.setText("");
        }
    }

}
