package sched;

public class Notes {
    public String developer;
    public String superintendent;
    public String lumber;

    Notes(Job job) {
        if (job.developer.equals("")) {
            this.developer = "";
        } else {
            this.developer = "DEV:" + job.name + "=" + job.developer;
        }

        if (job.superintendent.equals("")) {
            this.superintendent = "";
        } else {
            this.superintendent = "SUP:" + job.name + "=" + job.superintendent;
        }

        if (job.lumber.equals("")) {
            this.lumber = "";
        } else {
            this.lumber = "LUM:" + job.name + "=" + job.lumber;
        }
    }

    @Override
    public String toString() {
        return this.developer + "," + this.superintendent + "," + this.lumber;
    }
}
