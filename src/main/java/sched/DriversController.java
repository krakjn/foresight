package sched;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;

import java.util.Optional;

public class DriversController {
    @FXML ListView<String>   knownDriverListView;
    @FXML ListView<String>   driversListView;
    @FXML ListView<String>   liftTruckListView;
    @FXML RadioButton        truckTypeRentalRadioButton;
    @FXML RadioButton        truckTypeJLGRadioButton;
    @FXML RadioButton        truckTypeManatuRadioButton;
          ToggleGroup        truckTypeGroup;
    @FXML RadioButton        truckSize10RadioButton;
    @FXML RadioButton        truckSize12RadioButton;
          ToggleGroup        truckSizeGroup;
          private int        driverNum;

    public static SimpleIntegerProperty idx;
    public static SimpleIntegerProperty zeroDriver;


    public void initialize() {
        driverNum = 0;
        DriversController.idx = new SimpleIntegerProperty();
        DriversController.idx.set(-1);
        DriversController.zeroDriver = new SimpleIntegerProperty();
        DriversController.zeroDriver.set(0);
        knownDriverListView.setEditable(true);
        knownDriverListView.setCellFactory(TextFieldListCell.forListView());

        liftTruckListView.setEditable(true);
        liftTruckListView.setCellFactory(TextFieldListCell.forListView());

        truckTypeGroup = new ToggleGroup();
        truckTypeRentalRadioButton.setToggleGroup(truckTypeGroup);
        truckTypeJLGRadioButton.setToggleGroup(truckTypeGroup);
        truckTypeManatuRadioButton.setToggleGroup(truckTypeGroup);

        truckSizeGroup = new ToggleGroup();
        truckSize10RadioButton.setToggleGroup(truckSizeGroup);
        truckSize12RadioButton.setToggleGroup(truckSizeGroup);

        JobsController.idx.addListener((obsv, oldNumber, newNumber) -> refreshView());
        JobsController.zeroJob.addListener((obsv, before, after) -> refreshView());
        JobsController.zeroIdx.addListener((obsv, before, after) -> refreshView());
        zeroDriver.addListener((obsv, before, after) -> refreshView());


        // called after an edit or selection
        knownDriverListView.getSelectionModel().selectedItemProperty().addListener((obsv, oldName, newName) -> {
            knownDriverListView.layout(); // update UI
            int idx = knownDriverListView.getSelectionModel().getSelectedIndex();
            if (idx < 0) {
                knownDriverListView.getSelectionModel().select(knownDriverListView.getEditingIndex());
                return;
            }
            Foresight.knownDrivers.get(idx).name = newName;
        });

        knownDriverListView.setOnEditCommit(event -> {
            ObservableList<String> knownDriverList = knownDriverListView.getItems();
            int editIdx = knownDriverListView.getEditingIndex();
            knownDriverList.set(editIdx, event.getNewValue());
            knownDriverListView.getSelectionModel().select(editIdx);
        });


        // No edit needed for currentDrivers
        driversListView.getSelectionModel().selectedItemProperty().addListener((obsv, oldName, newName) -> {
            DriversController.idx.set(driversListView.getSelectionModel().getSelectedIndex());
        });


        // called after an edit or selection
        liftTruckListView.getSelectionModel().selectedItemProperty().addListener((obsv, oldName, newName) -> {
            liftTruckListView.layout(); // update UI
            int idx = liftTruckListView.getSelectionModel().getSelectedIndex();
            if (idx < 0) { //
                liftTruckListView.getSelectionModel().select(liftTruckListView.getEditingIndex());
                // selection causes re-entry
                return;
            }
            Optional<Job> optionalJob = JobsController.getCurrentJob();
            optionalJob.ifPresent(job -> {
                LiftTruck lt = job.liftTrucks.get(idx);
                lt.name = newName;  // model
                updateTruckType(lt.truckType);
                updateTruckSize(lt.truckSize);
            });
        });

        liftTruckListView.setOnEditCommit(event -> {
            ObservableList<String> liftTruckList = liftTruckListView.getItems();
            int editIdx = liftTruckListView.getEditingIndex();
            liftTruckList.set(editIdx, event.getNewValue());
            liftTruckListView.getSelectionModel().select(editIdx);
        });


        Parser.loaded.addListener((obsv, oldValue, newValue) -> {
            if (Foresight.knownDrivers.size() == 0) return; // nothing to populate
            ObservableList<String> knownList = knownDriverListView.getItems();
            knownList.clear();
            for (Driver driver: Foresight.knownDrivers) {
                knownList.add(driver.name);
            }
            knownDriverListView.getSelectionModel().selectFirst();
        });

    }

    public static int getIndex() {
        return DriversController.idx.get();
    }

    public static Optional<Driver> getCurrentDriver() {
        Optional<Job> optionalJob = JobsController.getCurrentJob();
        if (optionalJob.isPresent()) {
            Job job = optionalJob.get();
            int d = DriversController.getIndex();
            if (job.drivers.size() > 0 && d >= 0) {
                return Optional.of(job.drivers.get(d));
            }
        }
        return Optional.empty();

    }

    private void refreshView() {
        DriversController.idx.set(-1);  // to invalidate selection (unselect)
        ObservableList<String> driverList = driversListView.getItems();
        driverList.clear(); // always clear on job change
        ObservableList<String> truckList = liftTruckListView.getItems();
        truckList.clear();

        JobsController.getCurrentJob().ifPresent(job -> {
            for (Driver driver: job.drivers) {
                driverList.add(driver.name);
            }
            for (LiftTruck lt: job.liftTrucks) {
                truckList.add(lt.name);
            }
        });
    }

    @FXML
    private void addKnownDriver() {
        ObservableList<String> knownDriverList = knownDriverListView.getItems();
        knownDriverList.add("New Driver " + driverNum++);
        Foresight.knownDrivers.add(new Driver(driverNum));
        int newIdx = knownDriverList.size() - 1;
        knownDriverListView.getSelectionModel().select(newIdx);
        knownDriverListView.layout();
        knownDriverListView.edit(newIdx);
    }

    @FXML
    private void removeKnownDriver() {
        // TODO: if driver assigned to any other job should warn user
        // TODO: will not remove driver from any actions
        int idx = knownDriverListView.getSelectionModel().getSelectedIndex();
        if (idx >= 0) {
            knownDriverListView.getItems().remove(idx);
            Foresight.knownDrivers.remove(idx);
        }
    }

    @FXML
    private void editKnownDriver() {
        int idx = knownDriverListView.getSelectionModel().getSelectedIndex();
        if (idx >= 0) {
            knownDriverListView.edit(idx);
        }
    }

    @FXML
    private void addCurrentDriver() {
        int knownIndex = knownDriverListView.getSelectionModel().getSelectedIndex();
        if (knownIndex < 0) return;
        String knownDriver = knownDriverListView.getItems().get(knownIndex);
        for (String driver: driversListView.getItems()) {
            if (driver.equals(knownDriver)) {
                return; // already added!
            }
        }

        int f = ForemenController.getIndex();
        int j = JobsController.getIndex();
        if (f >= 0 && j >= 0) {
            // driver is tied to a job
            // a job can have many drivers
            // drivers span multiple phases
            // big jobs have multiple drivers
            driversListView.getItems().add(knownDriver); // update view
            Foresight.foremen.get(f).jobs.get(j).drivers.add(new Driver(knownDriver)); // update model
            driversListView.getSelectionModel().selectLast();
        }


    }

    @FXML
    private void remove() {
        int idx = driversListView.getSelectionModel().getSelectedIndex();
        if (idx >= 0) {
            JobsController.getCurrentJob().ifPresent(job -> {
                if (job.drivers.size() > 0) {
                    job.drivers.remove(idx);
                    driversListView.getItems().remove(idx);
                    if (driversListView.getItems().size() == 0) {
                        zeroDriver.set(zeroDriver.get() + 1);
                    }
                }
            });
        }

    }

    @FXML
    private void addLiftTruck() {
        Optional<Job> optionalJob = JobsController.getCurrentJob();
        optionalJob.ifPresent(job -> {
            job.liftTrucks.add(new LiftTruck(job.liftTruckNum));
            ObservableList<String> liftTruckList = liftTruckListView.getItems();
            liftTruckList.add("New Lift Truck " + job.liftTruckNum++);
            int newIdx = liftTruckList.size() - 1;
            liftTruckListView.getSelectionModel().select(newIdx);
            liftTruckListView.layout();
            liftTruckListView.edit(newIdx);
        });
    }

    @FXML
    private void removeLiftTruck() {
        Optional<Job> optionalJob = JobsController.getCurrentJob();
        optionalJob.ifPresent(job -> {
            int idx = liftTruckListView.getSelectionModel().getSelectedIndex();
            if (idx >= 0) {
                liftTruckListView.getItems().remove(idx);
                job.liftTrucks.remove(idx);
            }
        });
    }

    @FXML
    private void editLiftTruck() {
        Optional<Job> optionalJob = JobsController.getCurrentJob();
        optionalJob.ifPresent(job -> {
            int idx = liftTruckListView.getSelectionModel().getSelectedIndex();
            if (idx >= 0) {
                liftTruckListView.edit(idx);
            }
        });
    }


    private void updateTruckType(LiftTruck.TruckType truckType) {
        switch (truckType) {
            case RENTAL -> truckTypeRentalRadioButton.selectedProperty().setValue(true);
            case JLG -> truckTypeJLGRadioButton.selectedProperty().setValue(true);
            case MANATU -> truckTypeManatuRadioButton.selectedProperty().setValue(true);
            default -> {
                truckTypeRentalRadioButton.selectedProperty().setValue(false);
                truckTypeJLGRadioButton.selectedProperty().setValue(false);
                truckTypeManatuRadioButton.selectedProperty().setValue(false);
            }
        }
    }

    private void updateTruckSize(LiftTruck.TruckSize truckSize) {
        switch (truckSize) {
            case SIZE_10 -> truckSize10RadioButton.selectedProperty().setValue(true);
            case SIZE_12 -> truckSize12RadioButton.selectedProperty().setValue(true);
            default -> {
                truckSize10RadioButton.selectedProperty().setValue(false);
                truckSize12RadioButton.selectedProperty().setValue(false);
            }
        }
    }

    @FXML
    private void setLiftTruckType() {
        Optional<Job> optionalJob = JobsController.getCurrentJob();
        if (optionalJob.isPresent()) {
            int idx = liftTruckListView.getSelectionModel().getSelectedIndex();
            if (idx >= 0) {
                LiftTruck liftTruck = optionalJob.get().liftTrucks.get(idx);
                if (truckTypeRentalRadioButton.isSelected()) {
                    liftTruck.truckType = LiftTruck.TruckType.RENTAL;
                } else if (truckTypeJLGRadioButton.isSelected()) {
                    liftTruck.truckType = LiftTruck.TruckType.JLG;
                } else if (truckTypeManatuRadioButton.isSelected()) {
                    liftTruck.truckType = LiftTruck.TruckType.MANATU;
                }
            }
        } else {
            truckTypeRentalRadioButton.selectedProperty().setValue(false);
            truckTypeJLGRadioButton.selectedProperty().setValue(false);
            truckTypeManatuRadioButton.selectedProperty().setValue(false);
        }
    }

    @FXML
    private void setLiftTruckSize() {
        Optional<Job> optionalJob = JobsController.getCurrentJob();
        if (optionalJob.isPresent()) {
            int idx = liftTruckListView.getSelectionModel().getSelectedIndex();
            if (idx >= 0) {
                LiftTruck liftTruck = optionalJob.get().liftTrucks.get(idx);
                if (truckSize10RadioButton.isSelected()) {
                    liftTruck.truckSize = LiftTruck.TruckSize.SIZE_10;
                } else if (truckSize12RadioButton.isSelected()) {
                    liftTruck.truckSize = LiftTruck.TruckSize.SIZE_12;
                }
            }
        } else {
            truckSize10RadioButton.selectedProperty().setValue(false);
            truckSize12RadioButton.selectedProperty().setValue(false);
        }
    }

}
