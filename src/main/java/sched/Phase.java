package sched;

import java.time.LocalDate;
import java.util.ArrayList;

public class Phase {

    public String            name;
    public TimeFrame         driverTF;
    public ArrayList<Action> actions;
    public int               units;
    public String            notes;
    public TimeFrame         tf;

    Phase(int phaseNum) {
        this.name = "New Phase " + phaseNum;
        this.driverTF = new TimeFrame();
        this.actions = new ArrayList<>();
        this.units = 0;
        this.notes = "";
        this.tf = new TimeFrame();  // driven from lumber start and job complete
    }

    public void reorder(Action.Type type) {
        int counter = 1;
        for (Action action: actions) {
            if (action.type == type) {
                action.number = counter++;
            }
        }
    }

    public int getNumOfActions(Action.Type type) {

        if (type == Action.Type.JOB_COMPLETE) return 0;

        int numOfActions = 1;  // start one based
        for (Action action: actions) {
            if (action.type == type) {
                numOfActions++;
            }
        }
        return numOfActions;
    }

    public boolean driverIsWorking(LocalDate weeksEnd) {
        // weeksEnd is a Friday
        LocalDate sunday = weeksEnd.minusDays(5);
        LocalDate saturday = weeksEnd.plusDays(1);
        LocalDate start = this.driverTF.getStart();
        LocalDate end = this.driverTF.getEnd();

        // first the null check
        if (start == null || end == null) {
            return false;
        }

        // maybe pure conditional
        if (start.isAfter(sunday) && start.isBefore(saturday)) {
            return true; // start day within
        } else if (start.isBefore(sunday) && end.isAfter(saturday)) {
            return true; // right in the middle
        } else if (end.isAfter(sunday) && end.isBefore(saturday)) {
            return true; // end day within
        }

        return false;
    }

    public LocalDate getEarliestLumberDay(LocalDate newDay) {
        LocalDate earliestLumberDay = null;
        if (newDay != null) earliestLumberDay = newDay;

        for (Action a : actions) {
            if (a.day != null && a.type == Action.Type.LUMBER) {
                if (earliestLumberDay == null) {
                    earliestLumberDay = a.day;
                } else if (a.day.isBefore(earliestLumberDay)) {
                    earliestLumberDay = a.day;
                }
            }
        }
        return earliestLumberDay;  // can return NULL! (intended to blank out label)
    }
}
