package sched;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Optional;

/**  ...Java Notes...
 * List is an interface
 * ArrayList is a multithreaded vector
 * Vector is a single threaded vector
 */
public class Foreman {

    public enum County {
        NORTH,
        CENTRAL,
        SOUTH,
        INVALID;

        public short getColor() {
            short color;
            switch (this) {
                case NORTH -> color = IndexedColors.CORAL.index;
                case CENTRAL -> color = IndexedColors.AQUA.index;
                case SOUTH -> color = IndexedColors.PALE_BLUE.index;
                default -> color = IndexedColors.GREY_40_PERCENT.index;
            }
            return color;
        }
    }

    public String               name;
    public LocalDate            termination;       // endDate should not be set (unless termination)
    public County               county;
    public String               assistantName;

    /* Jobs have Phases
     * Phases have Actions
     * (Job, Phase) = Driver, NOTE: only one driver per job
     */
    public ArrayList<Job>       jobs;     // Each foreman is assigned to many jobs
    public int                  jobNum;
    public ArrayList<Vacation>  vacations;
    public int                  vacationNum;
    public HashMap<LocalDate, Action> viewMap; // view map has active actions


    Foreman(int foremanNum) {
        this.name = "New Foreman" + foremanNum;
        this.termination = null;
        this.county = County.INVALID;
        this.assistantName = "";
        this.jobs = new ArrayList<>();
        this.jobNum = 0;
        this.vacations = new ArrayList<>();
        this.vacationNum = 0;
        viewMap = new HashMap<>();

    }

    public void setCounty(String countyName) {
        switch (countyName) {
            case "NORTH" -> this.county = County.NORTH;
            case "CENTRAL" -> this.county = County.CENTRAL;
            case "SOUTH" -> this.county = County.SOUTH;
            default -> this.county = County.INVALID;
        }
    }

    public CellStyle getCellStyle(XSSFWorkbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFillForegroundColor(this.county.getColor());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setAlignment(HorizontalAlignment.RIGHT);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        return cellStyle;
    }

    public boolean onVacation(LocalDate today) {
        for (Vacation vacation: vacations) {

            LocalDate start = vacation.tf.getStart();
            LocalDate end = vacation.tf.getEnd();

            if (start != null && end != null) {

                if ((start.isEqual(today) || start.isBefore(today)) &&
                    (end.isEqual(today) || end.isAfter(today))) {

                    return true;

                }
            }
        }
        return false;
    }

    public boolean isWorking(LocalDate today) {
        int activeJobs = 0;
        boolean active;

        for (Job job: this.jobs) {
            for (Phase phase: job.phases) {

                active = false;  // reset after every phase
                // first see if we are started
                Optional<LocalDate> optStart = phase.tf.getOptStart();
                if (optStart.isPresent()) {
                    LocalDate startDay = optStart.get();
                    if (today.isEqual(startDay) || today.isAfter(startDay)) {
                        active = true;
                    }

                    // start date can be set without end date
                    Optional<LocalDate> optEnd = phase.tf.getOptEnd();
                    if (optEnd.isPresent()) {
                        LocalDate endDay = optEnd.get();
                        if (today.isAfter(endDay)) {
                            active = false;
                        }
                    }


                } else { // edge case, for phases without a start day

                    Optional<LocalDate> optEnd = phase.tf.getOptEnd();
                    if (optEnd.isPresent()) {
                        LocalDate endDay = optEnd.get();
                        // evaluates to false if today.isAfter(endDay), meaning job complete
                        active = today.isBefore(endDay) || today.isEqual(endDay);
                    }

                }

                if (active) activeJobs++;
            }
        }
        return (activeJobs > 0);
    }
}

class ForemanComparator implements Comparator<Foreman> {
    @Override
    public int compare(Foreman f1, Foreman f2) {
        //  result: -1 first, 0 equal, 1 last
        int result = f1.county.compareTo(f2.county);
        if (result == 0) {
            // sort on name
            result = f1.name.compareTo(f2.name);
        }
        return result;
    }
}
