package sched;


import java.time.LocalDate;

import javafx.beans.property.SimpleIntegerProperty;
import org.json.*;


public class Parser {

    public static SimpleIntegerProperty loaded = new SimpleIntegerProperty(0);


    public static void loadForesight(String rawJson) {
        // deserialize from json to java

        // model
        Foresight.foremen.clear(); // first clear out anything in memory, each foreman has a viewmap
        Foresight.knownDrivers.clear();

        // view should clear out and populate based on listeners

        String temp;  // helper string to parse LocalDates
        JSONTokener tokener = new JSONTokener(rawJson);

        while (tokener.more()) {
            try {
                var thing = tokener.nextValue();

                if (thing instanceof JSONObject) {

                    PrimaryController.author.set(((JSONObject) thing).getString("author"));
                    PrimaryController.viewStart.set(LocalDate.parse(((JSONObject) thing).getString("viewStart")));
                    PrimaryController.viewWeeks = ((JSONObject) thing).getInt("viewWeeks");

                    JSONArray foremenJsonArray = ((JSONObject) thing).getJSONArray("foremen");
                    for (int f = 0; f < foremenJsonArray.length(); f++) {
                        JSONObject foremanJsonObject = foremenJsonArray.getJSONObject(f);

                        Foreman buildForeman = new Foreman(f); // will overwrite name (hopefully)
                        buildForeman.name = foremanJsonObject.getString("name");
                        buildForeman.setCounty(foremanJsonObject.getString("county"));
                        buildForeman.assistantName = foremanJsonObject.getString("assistant");
                        temp = foremanJsonObject.getString("termination");
                        if (!temp.equals("")) buildForeman.termination = LocalDate.parse(temp);

                        JSONArray vacationsJsonArray = foremanJsonObject.getJSONArray("vacations");
                        for (int v = 0; v < vacationsJsonArray.length(); v++) {
                            JSONObject vacationJsonObject = vacationsJsonArray.getJSONObject(v);
                            Vacation buildVacation = new Vacation(v);
                            buildVacation.name = vacationJsonObject.getString("name");
                            temp = vacationJsonObject.getString("tfStart");
                            if (!temp.equals("")) buildVacation.tf.setStart(LocalDate.parse(temp));
                            temp = vacationJsonObject.getString("tfEnd");
                            if (!temp.equals("")) buildVacation.tf.setEnd(LocalDate.parse(temp));

                            buildForeman.vacations.add(buildVacation);
                        }


                        JSONArray jobsJsonArray = foremanJsonObject.getJSONArray("jobs");
                        for (int j = 0; j < jobsJsonArray.length(); j++) {
                            JSONObject jobJsonObject = jobsJsonArray.getJSONObject(j);
                            Job buildJob = new Job(j);
                            buildJob.name = jobJsonObject.getString("name");
                            buildJob.developer = jobJsonObject.getString("developer");
                            buildJob.superintendent = jobJsonObject.getString("superintendent");
                            buildJob.lumber = jobJsonObject.getString("lumber");
                            buildJob.notes = jobJsonObject.getString("notes");


                            JSONArray driversJsonArray = jobJsonObject.getJSONArray("drivers");
                            for (int d = 0; d < driversJsonArray.length(); d++) {
                                JSONObject driverJsonObject = driversJsonArray.getJSONObject(d);
                                Driver buildDriver = new Driver(d);
                                buildDriver.name = driverJsonObject.getString("name");

                                buildJob.drivers.add(buildDriver);
                            }


                            JSONArray liftTrucksJsonArray = jobJsonObject.getJSONArray("liftTrucks");
                            for (int t = 0; t < liftTrucksJsonArray.length(); t++) {
                                JSONObject liftTruckJsonObject = liftTrucksJsonArray.getJSONObject(t);
                                LiftTruck buildLiftTruck = new LiftTruck(t);
                                buildLiftTruck.name = liftTruckJsonObject.getString("name");
                                buildLiftTruck.setTruckType(liftTruckJsonObject.getString("type"));
                                buildLiftTruck.setTruckSize(liftTruckJsonObject.getString("size"));

                                buildJob.liftTrucks.add(buildLiftTruck);
                            }


                            JSONArray phasesJsonArray = jobJsonObject.getJSONArray("phases");
                            for (int p = 0; p < phasesJsonArray.length(); p++) {
                                JSONObject phaseJsonObject = phasesJsonArray.getJSONObject(p);
                                Phase buildPhase = new Phase(p);
                                buildPhase.name = phaseJsonObject.getString("name");
                                temp = phaseJsonObject.getString("driverStart");
                                if (!temp.equals("")) buildPhase.driverTF.setStart(LocalDate.parse(temp));
                                temp = phaseJsonObject.getString("driverEnd");
                                if (!temp.equals("")) buildPhase.driverTF.setEnd(LocalDate.parse(temp));
                                buildPhase.units = phaseJsonObject.getInt("units");
                                buildPhase.notes = phaseJsonObject.getString("notes");
                                temp = phaseJsonObject.getString("tfStart");
                                if (!temp.equals("")) buildPhase.tf.setStart(LocalDate.parse(temp));
                                temp = phaseJsonObject.getString("tfEnd");
                                if (!temp.equals("")) buildPhase.tf.setEnd(LocalDate.parse(temp));


                                JSONArray actionsJsonArray = phaseJsonObject.getJSONArray("actions");

                                for (int a = 0; a < actionsJsonArray.length(); a++) {
                                    JSONObject actionJsonObject = actionsJsonArray.getJSONObject(a);
                                    Action buildAction;
                                    int actionNumber = actionJsonObject.getInt("number");
                                    temp = actionJsonObject.getString("type");
                                    switch (temp) {
                                        case "TRENCH" ->    buildAction = new Action(Action.Type.TRENCH, actionNumber, "");
                                        case "SLABS" ->     buildAction = new Action(Action.Type.SLABS , actionNumber, "");
                                        case "LUMBER" ->    buildAction = new Action(Action.Type.LUMBER, actionNumber, "");
                                        case "SNAP" ->      buildAction = new Action(Action.Type.SNAP  , actionNumber, "");
                                        case "LAYOUT" ->    buildAction = new Action(Action.Type.LAYOUT, actionNumber, "");
                                        case "ROOFSHEET" -> buildAction = new Action(Action.Type.ROOFSHEET, actionNumber, "");
                                        case "JOB_COMPLETE" -> buildAction = new Action(Action.Type.JOB_COMPLETE, 0, "");
                                        default -> buildAction = new Action(Action.Type.INVALID, 0, "");
                                    }
                                    buildAction.actioneer = actionJsonObject.getString("actioneer");
                                    buildAction.lumberDriver = actionJsonObject.getString("lumberDriver");
                                    temp = actionJsonObject.getString("day");
                                    if (!temp.equals("")) buildAction.day = LocalDate.parse(temp);
                                    buildAction.notes = actionJsonObject.getString("actioneer");
                                    buildAction.fqn = actionJsonObject.getString("fqn");

                                    // Each foreman has a view map
                                    buildForeman.viewMap.put(buildAction.day, buildAction);

                                    buildPhase.actions.add(buildAction);
                                }

                                buildJob.phases.add(buildPhase);
                            }

                            buildForeman.jobs.add(buildJob);
                        }

                        Foresight.foremen.add(buildForeman);
                    }
                    // KNOWN DRIVERS lives outside foreman
                    JSONArray knownDriverJsonArray = ((JSONObject) thing).getJSONArray("knownDrivers");
                    for (int k = 0; k < knownDriverJsonArray.length(); k++) {
                        JSONObject knownDriverJsonObject = knownDriverJsonArray.getJSONObject(k);
                        Driver buildKnownDriver = new Driver(k);
                        buildKnownDriver.name = knownDriverJsonObject.getString("name");

                        Foresight.knownDrivers.add(buildKnownDriver);
                    }

                }

            } catch (JSONException exception) {
                Foresight.warnUserDialog("JSON Exception",
                        "Unable to parse embedded data: " + exception.toString());
                break;
            }

        }

        loaded.set(loaded.get() + 1);
    }

}
