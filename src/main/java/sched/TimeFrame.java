package sched;

import java.time.LocalDate;
import java.util.Optional;

public class TimeFrame {

    /**
     * This is under the assumption this is all based on
     * Pacific Standard Time UTC-09:00
     */
    private LocalDate start;
    private LocalDate end;


    TimeFrame() {}

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }


    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public Optional<LocalDate> getOptStart() {
        if (this.start != null) {
            return Optional.of(this.start);
        } else {
            return Optional.empty();
        }
    }

    public Optional<LocalDate> getOptEnd() {
        if (this.end != null) {
            return Optional.of(this.end);
        } else {
            return Optional.empty();
        }
    }

}
