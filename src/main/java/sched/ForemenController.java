package sched;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;

import java.util.Optional;

public class ForemenController {

    @FXML ListView<String> foremenListView;
    @FXML RadioButton   northCountyRadioButton;
    @FXML RadioButton   centralCountyRadioButton;
    @FXML RadioButton   southCountyRadioButton;
          ToggleGroup   countyGroup;
    @FXML TextField     assistantTextField;
    @FXML DatePicker    terminationDatePicker;
    @FXML ListView<String> vacationListView;
    @FXML DatePicker    vacationStartDatePicker;
    @FXML DatePicker    vacationEndDatePicker;

          int           foremenNum;

    public static SimpleIntegerProperty idx;  // index of current selected foreman
    public static SimpleIntegerProperty zeroForeman;
    public static SimpleIntegerProperty zeroIdx;

    public static SimpleIntegerProperty zeroVacation;
    public static SimpleIntegerProperty zeroVacationIdx;

    /**
     * Any Controller with this method will get called and loading of FXML file
     * which has access to the fxml bindings
     */
    public void initialize() {
        //setup listview
        foremenNum = 0;
        ForemenController.idx = new SimpleIntegerProperty();
        ForemenController.idx.set(-1);
        ForemenController.zeroForeman = new SimpleIntegerProperty();
        ForemenController.zeroForeman.set(0);
        ForemenController.zeroIdx = new SimpleIntegerProperty();
        ForemenController.zeroIdx.set(0);

        ForemenController.zeroVacation = new SimpleIntegerProperty();
        ForemenController.zeroVacation.set(0);
        ForemenController.zeroVacationIdx = new SimpleIntegerProperty();
        ForemenController.zeroVacationIdx.set(0);


        foremenListView.setCellFactory(TextFieldListCell.forListView());
        foremenListView.setEditable(true);  // false by default

        // setup toggle group for radio buttons
        countyGroup = new ToggleGroup();
        northCountyRadioButton.setToggleGroup(countyGroup);
        centralCountyRadioButton.setToggleGroup(countyGroup);
        southCountyRadioButton.setToggleGroup(countyGroup);

        vacationListView.setCellFactory(TextFieldListCell.forListView());
        vacationListView.setEditable(true);  // false by default

        zeroForeman.addListener((obsv, before, after) -> resetView());
        zeroIdx.addListener((obsv, before, after) -> updateView());

        zeroVacation.addListener((obsv, before, after) -> resetVacationView());
        zeroVacationIdx.addListener((obsv, before, after) -> updateVacationView());

        // This is called after and selection/edit of the list cell
        foremenListView.getSelectionModel().selectedItemProperty().addListener(
            (observableValue, oldName, newName) -> {
                // first set the universal index (other listeners will pick up event)
                int idx = foremenListView.getSelectionModel().getSelectedIndex();
                if (idx < 0) { // edits come in as -1
                    foremenListView.getSelectionModel().select(foremenListView.getEditingIndex());
                    // selection causes re-entry
                    return;
                }
                ForemenController.idx.set(idx);
                ForemenController.getCurrentForeman().ifPresent(foreman -> foreman.name = newName);
                updateView();
            }
        );

        foremenListView.setOnEditCommit(event -> {
            ObservableList<String> foremanList = foremenListView.getItems();
            int editIdx = foremenListView.getEditingIndex();
            foremanList.set(editIdx, event.getNewValue());
            foremenListView.getSelectionModel().select(editIdx);
        });

        // This is called after and selection/edit of the list cell
        vacationListView.getSelectionModel().selectedItemProperty().addListener(
            (observableValue, oldName, newName) -> {
                // first set the universal index (other listeners will pick up event)
                vacationListView.layout();
                int idx = vacationListView.getSelectionModel().getSelectedIndex();
                if (idx < 0) { // edits come in as -1
                    vacationListView.getSelectionModel().select(vacationListView.getEditingIndex());
                    // selection causes re-entry
                    return;
                }
                getCurrentForeman().ifPresent(foreman -> {
                    Vacation vacation = foreman.vacations.get(idx);
                    vacation.name = newName;
                    vacationStartDatePicker.setValue(vacation.tf.getStart());
                    vacationEndDatePicker.setValue(vacation.tf.getEnd());
                });
            }
        );

        vacationListView.setOnEditCommit(event -> {
            ObservableList<String> vacationList = vacationListView.getItems();
            int editIdx = vacationListView.getEditingIndex();
            vacationList.set(editIdx, event.getNewValue());
            vacationListView.getSelectionModel().select(editIdx);
        });

        Parser.loaded.addListener((observableValue, oldValue, newValue) -> {
            ObservableList<String> foremanList = foremenListView.getItems();
            foremanList.clear();
            foremenNum = 0;
            for (Foreman foreman: Foresight.foremen) {
                foremanList.add(foreman.name);
            }
            // cascading selection from listeners
            foremenListView.getSelectionModel().selectFirst();
        });

    }

    public static int getIndex() {
        return idx.get();
    }

    public static Optional<Foreman> getCurrentForeman() {
        int idx = ForemenController.getIndex();
        if (idx >= 0 ) {
            if (Foresight.foremen.size() > 0) {
                return Optional.of(Foresight.foremen.get(idx));
            }
        }
        return Optional.empty();
    }

    private void resetView() {
        // invalidate
        foremenListView.getItems().clear();
        updateCounty(Foreman.County.INVALID);
        assistantTextField.setText("");
        terminationDatePicker.setValue(null);
        vacationListView.getItems().clear();
        vacationStartDatePicker.setValue(null);
        vacationEndDatePicker.setValue(null);
    }

    private void updateView() {
        getCurrentForeman().ifPresent(foreman -> {
            updateCounty(foreman.county);
            assistantTextField.setText(foreman.assistantName);
            terminationDatePicker.setValue(foreman.termination);
            vacationListView.getItems().clear();
            for (Vacation vacation: foreman.vacations) {
                vacationListView.getItems().add(vacation.name);
            }
            if (foreman.vacations.size() > 0) { // select first vacation
                vacationListView.getSelectionModel().selectFirst();
            } else {
                vacationStartDatePicker.setValue(null); // no vacations loaded
                vacationEndDatePicker.setValue(null);
            }
        });
    }


    /**
     * Need to select THEN edit
     */
    @FXML private void add() {
        ObservableList<String> foremanList = foremenListView.getItems();
        foremanList.add("New Foreman " + foremenNum);     // update view
        Foresight.foremen.add(new Foreman(foremenNum++)); // update model
        int newIdx = foremanList.size() - 1;
        foremenListView.getSelectionModel().select(newIdx);
        foremenListView.layout();
        foremenListView.edit(newIdx);
    }

    @FXML private void remove() {
        int idx = foremenListView.getSelectionModel().getSelectedIndex();
        if (idx >= 0) {
            Foresight.foremen.remove(idx); // model
            foremenListView.getItems().remove(idx); // view
            if (foremenListView.getItems().size() == 0) {
                // for either no jobs loaded OR the zero index job removed
                // doesn't update the index, so need to update the value manually
                zeroForeman.set(zeroForeman.get() + 1);
            } else if (idx == 0) {
                zeroIdx.set(zeroIdx.get() + 1); // to help others update
            }
        }
    }

    @FXML private void edit() {
        // listener will update data
        foremenListView.edit(ForemenController.getIndex());
    }

    @FXML
    private void setCounty() {
        Optional<Foreman> optionalForeman = getCurrentForeman();
        if (optionalForeman.isPresent()) {
            if (northCountyRadioButton.isSelected()) {
                optionalForeman.get().county = Foreman.County.NORTH;
            } else if (centralCountyRadioButton.isSelected()) {
                optionalForeman.get().county = Foreman.County.CENTRAL;
            } else if (southCountyRadioButton.isSelected()) {
                optionalForeman.get().county = Foreman.County.SOUTH;
            }
        } else {
            northCountyRadioButton.selectedProperty().setValue(false);
            centralCountyRadioButton.selectedProperty().setValue(false);
            southCountyRadioButton.selectedProperty().setValue(false);
        }
    }

    private void updateCounty(Foreman.County county) {
        if (county == Foreman.County.NORTH) {
            northCountyRadioButton.selectedProperty().setValue(true);
        } else if (county == Foreman.County.CENTRAL) {
            centralCountyRadioButton.selectedProperty().setValue(true);
        } else if (county == Foreman.County.SOUTH) {
            southCountyRadioButton.selectedProperty().setValue(true);
        } else {
            northCountyRadioButton.selectedProperty().setValue(false);
            centralCountyRadioButton.selectedProperty().setValue(false);
            southCountyRadioButton.selectedProperty().setValue(false);
        }
    }

    @FXML
    private void setAssistant(/*ActionEvent e*/) {
        Optional<Foreman> optionalForeman = getCurrentForeman();
        if (optionalForeman.isPresent()) {
            optionalForeman.get().assistantName = assistantTextField.getText();
        } else {
            assistantTextField.setText("");
        }
    }

    @FXML
    private void setTermination() {
        Optional<Foreman> optionalForeman = getCurrentForeman();
        if (optionalForeman.isPresent()) {
            optionalForeman.get().termination = terminationDatePicker.getValue();
        } else {
            terminationDatePicker.setValue(null);
        }
    }

    private void resetVacationView() {
        vacationListView.getItems().clear();
        vacationStartDatePicker.setValue(null);
        vacationEndDatePicker.setValue(null);
    }

    private void updateVacationView() {
        getCurrentForeman().ifPresent(foreman -> {
            Vacation vacation = foreman.vacations.get(vacationListView.getSelectionModel().getSelectedIndex());
            vacationStartDatePicker.setValue(vacation.tf.getStart());
            vacationEndDatePicker.setValue(vacation.tf.getEnd());
        });
    }

    @FXML private void vacationAdd() {
        // make sure to add THEN select THEN edit
        getCurrentForeman().ifPresent(foreman -> {
            ObservableList<String> vacationList = vacationListView.getItems();
            vacationList.add("New Vacation " + foreman.vacationNum);
            foreman.vacations.add(new Vacation(foreman.vacationNum++));
            int newIdx = vacationList.size() - 1;
            vacationListView.getSelectionModel().select(newIdx);
            vacationListView.layout();
            vacationListView.edit(newIdx);
        });
    }

    @FXML private void vacationRemove() {
        int idx = vacationListView.getSelectionModel().getSelectedIndex();
        if (idx >= 0) {
            getCurrentForeman().ifPresent(foreman -> {
                vacationListView.getItems().remove(idx); // view
                foreman.vacations.remove(idx); // model
                if (vacationListView.getItems().size() == 0) {
                    zeroVacation.set(zeroVacation.get() + 1);
                } else if (idx == 0) {
                    zeroVacationIdx.set(zeroVacationIdx.get() + 1);
                }
            });
        }
    }

    @FXML private void vacationEdit() {
        // listener will update data
        int idx = vacationListView.getSelectionModel().getSelectedIndex();
        if (idx >= 0) {
            vacationListView.edit(idx);
        }
    }


    @FXML
    private void setVacationStart() {
        Optional<Foreman> optionalForeman = getCurrentForeman();
        if (optionalForeman.isPresent()) {
            int idx = vacationListView.getSelectionModel().getSelectedIndex();
            if (idx >= 0) {
                Vacation vacation = optionalForeman.get().vacations.get(idx);
                vacation.tf.setStart(vacationStartDatePicker.getValue());
            }
        } else {
            vacationStartDatePicker.setValue(null);
        }
    }

    @FXML
    private void setVacationEnd() {
        Optional<Foreman> optionalForeman = getCurrentForeman();
        if (optionalForeman.isPresent()) {
            int idx = vacationListView.getSelectionModel().getSelectedIndex();
            if (idx >= 0) {
                Vacation vacation = optionalForeman.get().vacations.get(idx);
                vacation.tf.setEnd(vacationEndDatePicker.getValue());
            }
        } else {
            vacationEndDatePicker.setValue(null);
        }
    }

}
