package sched;

public class Vacation {
    public String name;
    public TimeFrame tf;

    Vacation(int vacationNum) {
        this.name = "New Vacation " + vacationNum;
        this.tf = new TimeFrame();
    }
}
