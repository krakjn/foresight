package sched;


import org.apache.poi.ss.usermodel.*;

import java.time.LocalDate;

public class Action {

    public static final CellStyle trenchStyle = Excel.workbook.createCellStyle();
    public static final CellStyle slabStyle = Excel.workbook.createCellStyle();
    public static final CellStyle lumberStyle = Excel.workbook.createCellStyle();
    public static final CellStyle snapStyle = Excel.workbook.createCellStyle();
    public static final CellStyle layoutStyle = Excel.workbook.createCellStyle();
    public static final CellStyle roofsheetStyle = Excel.workbook.createCellStyle();
    public static final CellStyle completeStyle = Excel.workbook.createCellStyle();


    public enum Type {
        TRENCH,
        SLABS,
        LUMBER,  // START JOB
        SNAP,
        LAYOUT,
        ROOFSHEET,
        JOB_COMPLETE,  // END JOB
        INVALID;

        public String toViewString() {
            switch (this) {
                case TRENCH -> { return "Trench"; }
                case SLABS -> { return "Slabs"; }
                case LUMBER -> { return "Lumber"; }
                case SNAP -> { return "Snap"; }
                case LAYOUT -> { return "Layout"; }
                case ROOFSHEET -> { return "Roofsheet"; }
                case JOB_COMPLETE -> { return "Job Complete"; }
                default -> {return "INVALID"; }
            }
        }

        public Type from(String str) {
            switch (str) {
                case "Trench" -> { return Type.TRENCH; }
                case "Slabs" -> { return Type.SLABS; }
                case "Lumber" -> { return Type.LUMBER; }
                case "Snap" -> { return Type.SNAP; }
                case "Layout" -> { return Type.LAYOUT; }
                case "Roofsheet" -> { return Type.ROOFSHEET; }
                case "Job Complete" -> { return Type.JOB_COMPLETE; }
                default -> { return Type.INVALID; }
            }
        }

        public String toExcelString() {
            switch (this) {
                case TRENCH ->    { return "TR"; }
                case SLABS ->     { return "SL"; }
                case LUMBER ->    { return "LM"; }
                case SNAP ->      { return "SN"; }
                case LAYOUT ->    { return "LY"; }
                case ROOFSHEET -> { return "RF"; }
                case JOB_COMPLETE -> { return "CP"; }
                default ->        { return "INVALID"; }
            }
        }

    }

    public Type      type;
    public int       number;
    public String    actioneer;
    public String    lumberDriver;  // for special action
    public LocalDate day;
    public String    notes;
    public CellStyle style;
    public String    fqn;
    public String    viewName;


    Action(Type type, int num, String lumberDriver) {
        this.type = type;
        this.number = num;
        this.actioneer = "";
        this.lumberDriver = lumberDriver;
        this.day = null;
        this.notes = "";
        switch (this.type) {
            case TRENCH ->  this.style = trenchStyle;
            case SLABS -> this.style = slabStyle;
            case LUMBER -> this.style = lumberStyle;
            case SNAP -> this.style = snapStyle;
            case LAYOUT -> this.style = layoutStyle;
            case ROOFSHEET -> this.style = roofsheetStyle;
            case JOB_COMPLETE -> this.style = completeStyle;
            default -> this.style = null;
        }
        this.fqn = "";
        this.viewName = "";
    }

    public String getViewName() {
        if (this.type == Type.JOB_COMPLETE) {
            return this.type.toViewString();
        } else {
            return type.toViewString() + " " + this.number;
        }
    }

//    public void setViewName(int newNumber) {
//        if (this.type != Type.JOB_COMPLETE) {
//            this.viewName = this.type.toViewString() + " " + newNumber;
//        }
//    }

    public void setNumber(int newNumber) {
        this.number = newNumber;
    }

    public String getExcelName() {
        String retval;
        if (this.number > 1) {
            retval = this.type.toExcelString() + this.number;
        } else {
            retval = this.type.toExcelString();
        }
        return retval;
    }

    public LocalDate getDay() {
        return this.day;
    }

    public void setDay(LocalDate day) {
        this.day = day;
    }

    public boolean isDateSet() {
        return this.day != null;
    }

    public static void initStyles() {
        /*
         * need color code for actions
         * --orange,green--
         * trench - brown
         * slabs - gray
         * snap - pink
         * layout - light purple
         * roofsheet - sky blue
         * lumber - green/tan....
         *
         * vacation hatched (style the cell with lines)
         */
        trenchStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        trenchStyle.setFillForegroundColor(IndexedColors.TAN.index);
        trenchStyle.setAlignment(HorizontalAlignment.CENTER);
        trenchStyle.setBorderLeft(BorderStyle.THIN);
        trenchStyle.setBorderRight(BorderStyle.THIN);
        trenchStyle.setBorderTop(BorderStyle.THIN);
        trenchStyle.setBorderBottom(BorderStyle.THIN);

        slabStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        slabStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        slabStyle.setAlignment(HorizontalAlignment.CENTER);
        slabStyle.setBorderLeft(BorderStyle.THIN);
        slabStyle.setBorderRight(BorderStyle.THIN);
        slabStyle.setBorderTop(BorderStyle.THIN);
        slabStyle.setBorderBottom(BorderStyle.THIN);

        lumberStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        lumberStyle.setFillForegroundColor(IndexedColors.SEA_GREEN.index);
        lumberStyle.setAlignment(HorizontalAlignment.CENTER);
        lumberStyle.setBorderLeft(BorderStyle.THIN);
        lumberStyle.setBorderRight(BorderStyle.THIN);
        lumberStyle.setBorderTop(BorderStyle.THIN);
        lumberStyle.setBorderBottom(BorderStyle.THIN);

        snapStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        snapStyle.setFillForegroundColor(IndexedColors.ROSE.index); // maybe pink?
        snapStyle.setAlignment(HorizontalAlignment.CENTER);
        snapStyle.setBorderLeft(BorderStyle.THIN);
        snapStyle.setBorderRight(BorderStyle.THIN);
        snapStyle.setBorderTop(BorderStyle.THIN);
        snapStyle.setBorderBottom(BorderStyle.THIN);


        layoutStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        layoutStyle.setFillForegroundColor(IndexedColors.LAVENDER.index);
        layoutStyle.setAlignment(HorizontalAlignment.CENTER);
        layoutStyle.setBorderLeft(BorderStyle.THIN);
        layoutStyle.setBorderRight(BorderStyle.THIN);
        layoutStyle.setBorderTop(BorderStyle.THIN);
        layoutStyle.setBorderBottom(BorderStyle.THIN);


        roofsheetStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        roofsheetStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.index);
        roofsheetStyle.setAlignment(HorizontalAlignment.CENTER);
        roofsheetStyle.setBorderLeft(BorderStyle.THIN);
        roofsheetStyle.setBorderRight(BorderStyle.THIN);
        roofsheetStyle.setBorderTop(BorderStyle.THIN);
        roofsheetStyle.setBorderBottom(BorderStyle.THIN);

        Font completeFont = Excel.workbook.createFont();
        completeFont.setColor(IndexedColors.WHITE.index);
        completeFont.setBold(true);
        completeStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        completeStyle.setFillForegroundColor(IndexedColors.MAROON.index);
        completeStyle.setFont(completeFont);
        completeStyle.setAlignment(HorizontalAlignment.CENTER);
        completeStyle.setBorderLeft(BorderStyle.THIN);
        completeStyle.setBorderRight(BorderStyle.THIN);
        completeStyle.setBorderTop(BorderStyle.THIN);
        completeStyle.setBorderBottom(BorderStyle.THIN);

    }

}
