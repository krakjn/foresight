package sched;

import java.time.LocalDate;
import java.util.ArrayList;

public class Job {
    public String               name;
    public TimeFrame            tf;
    public ArrayList<Phase>     phases; // phases have actions
    public int                  phaseNum;
    public ArrayList<Driver>    drivers;
    public ArrayList<LiftTruck> liftTrucks; // drivers use trucks they do not belong to driver
    public int                  liftTruckNum;
    public String               notes;
    public String               developer;
    public String               superintendent;
    public String               lumber;

    Job(int jobNum) {
        this.name = "New Job " + jobNum;
        this.tf = new TimeFrame();
        this.drivers = new ArrayList<>();
        this.phases = new ArrayList<>();
        this.phaseNum = 0;
        this.liftTrucks = new ArrayList<>();
        this.liftTruckNum = 0;
        this.notes = "";
        this.developer = "";
        this.superintendent = "";
        this.lumber = "";
    }

}
