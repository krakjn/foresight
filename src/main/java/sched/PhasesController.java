package sched;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;

import java.time.LocalDate;
import java.util.Optional;

import static sched.JobsController.getCurrentJob;

public class PhasesController {
    // For the phase FXML
    @FXML public ListView<String>   phasesListView;
    @FXML public Label              phaseStartLabel;
    @FXML public Label              phaseEndLabel;
    @FXML public TextField          phaseUnitsTextField;
    @FXML public DatePicker         driverStartDatePicker;
    @FXML public DatePicker         driverEndDatePicker;

    @FXML public TextArea           notesTextArea;

    public static SimpleIntegerProperty  idx;
    public static SimpleIntegerProperty  zeroPhase;
    public static SimpleIntegerProperty  zeroIdx;

    // css can belong to class
    final PseudoClass errorClass = PseudoClass.getPseudoClass("error");
    final PseudoClass startClass = PseudoClass.getPseudoClass("start");
    final PseudoClass completeClass = PseudoClass.getPseudoClass("complete");

    public void initialize() {
        PhasesController.idx = new SimpleIntegerProperty();
        PhasesController.idx.set(-1);
        PhasesController.zeroPhase = new SimpleIntegerProperty();
        PhasesController.zeroPhase.set(0);
        PhasesController.zeroIdx = new SimpleIntegerProperty();
        PhasesController.zeroIdx.set(0);

        phasesListView.setCellFactory(TextFieldListCell.forListView());
        phasesListView.setEditable(true);
        phaseUnitsTextField.setEditable(true);

        phaseStartLabel.pseudoClassStateChanged(startClass, true);
        phaseEndLabel.pseudoClassStateChanged(completeClass, true);

        // need to initialize actions listeners
        ActionsController.lumberDate = new SimpleObjectProperty<>();
        ActionsController.completeDate = new SimpleObjectProperty<>();


        // Listen for Job changes
        JobsController.idx.addListener((observableValue, oldNumber, newNumber) -> refreshView());
        JobsController.zeroJob.addListener((obsv, before, after) -> refreshView());
        JobsController.zeroIdx.addListener((obsv, before, after) -> refreshView());
        zeroPhase.addListener((obsv, before, after) -> refreshView());

        phasesListView.getSelectionModel().selectedItemProperty().addListener(
            (obsv, oldName, newName) -> {
                phasesListView.layout();
                int idx = phasesListView.getSelectionModel().getSelectedIndex();
                if (idx < 0) {
                    phasesListView.getSelectionModel().select(phasesListView.getEditingIndex());
                    // selection causes re-entry
                    return;
                }
                PhasesController.idx.set(idx);
                getCurrentPhase().ifPresent((phase) -> {
                    phase.name = newName;
                    phaseUnitsTextField.setText(phase.units == 0 ? "" : String.valueOf(phase.units));
                    phaseUnitsTextField.pseudoClassStateChanged(errorClass, false);
                    driverStartDatePicker.setValue(phase.driverTF.getStart());
                    driverEndDatePicker.setValue(phase.driverTF.getEnd());

                    phaseStartLabel.setText(getDateString(phase.tf.getStart()));
                    phaseEndLabel.setText(getDateString(phase.tf.getEnd()));

                    notesTextArea.setText(phase.notes);
                    // update all actions fqns
                    for (Action action: phase.actions) {
                        action.fqn = getCurrentFQN();
                    }
                });
            }
        );

        phasesListView.setOnEditCommit(event -> {
            ObservableList<String> phasesList = phasesListView.getItems();
            int editIdx = phasesListView.getEditingIndex();
            phasesList.set(editIdx, event.getNewValue());
            phasesListView.getSelectionModel().select(editIdx);
        });

        ActionsController.lumberDate.addListener((obsv, oldValue, newValue) -> {
            phaseStartLabel.setText(getDateString(newValue));
        });

        ActionsController.completeDate.addListener((obsv, oldValue, newValue) -> {
            phaseEndLabel.setText(getDateString(newValue));
        });


        Parser.loaded.addListener((obsv, oldValue, newValue) -> {
            ObservableList<String> phasesList = phasesListView.getItems();
            phasesList.clear();
            getCurrentJob().ifPresent(job -> {
                for (Phase phase: job.phases) {
                    phasesList.add(phase.name);
                }
            });
            // cascading selection from listeners
            phasesListView.getSelectionModel().selectFirst();
        });

    }

    public static int getIndex() {
        return PhasesController.idx.get();
    }

    public static Optional<Phase> getCurrentPhase() {
        Optional<Job> optionalJob = JobsController.getCurrentJob();
        if (optionalJob.isPresent()) {
            Job job = optionalJob.get();
            int p = PhasesController.getIndex();
            if (job.phases.size() > 0 && p >= 0) {
                return Optional.of(job.phases.get(p));
            }
        }
        return Optional.empty();
    }

    private void refreshView() {
        ObservableList<String> phasesList = phasesListView.getItems();
        phasesList.clear();
        PhasesController.idx.set(-1); // to invalidate
        JobsController.getCurrentJob().ifPresent(job -> {
            for (Phase phase : job.phases) {
                phasesList.add(phase.name);
            }
        });
        // clear fields
        phaseStartLabel.setText("---");
        phaseEndLabel.setText("---");
        phaseUnitsTextField.setText("");
        driverStartDatePicker.setValue(null);
        driverEndDatePicker.setValue(null);
        notesTextArea.setText("");
    }

    @FXML
    private void add() {
        int f = ForemenController.getIndex();
        int j = JobsController.getIndex();
        if (f >= 0 && j >= 0) {
            Job job = Foresight.foremen.get(f).jobs.get(j);
            job.phases.add(new Phase(job.phaseNum));
            ObservableList<String> phasesList = phasesListView.getItems();
            phasesList.add("New Phase " + job.phaseNum++);
            int newIdx = phasesList.size() - 1;
            phasesListView.getSelectionModel().select(newIdx);
            phasesListView.layout();
            phasesListView.edit(newIdx);
        }
    }

    @FXML
    private void remove() {
        int f = ForemenController.getIndex();
        int j = JobsController.getIndex();
        int p = PhasesController.getIndex();
        if (f >= 0 && j >= 0 && p >= 0) {
            if (Foresight.foremen.get(f).jobs.get(j).phases.size() > 0) {
                Foresight.foremen.get(f).jobs.get(j).phases.remove(p);
                phasesListView.getItems().remove(p);
                if (phasesListView.getItems().size() == 0) {
                    zeroPhase.set(zeroPhase.get() + 1);
                } else if (p == 0) {
                    zeroIdx.set(zeroIdx.get() + 1); // to help others update
                }
            }
        }

    }

    @FXML
    private void edit() {
        int p = PhasesController.getIndex();
        if (p >= 0) {
            phasesListView.edit(p);
        }
    }

    @FXML
    private void setLotsUnits() {
        Optional<Phase> optionalPhase = getCurrentPhase();
        if (optionalPhase.isPresent()) {
            Phase phase = optionalPhase.get();
            String lotsUnitsStr = phaseUnitsTextField.getText();
            try {
                phase.units = lotsUnitsStr.equals("") ? 0 : Integer.parseInt(lotsUnitsStr);
                for (Action action: phase.actions) {
                    action.fqn = getCurrentFQN(); // update fqn for each action
                }
                phaseUnitsTextField.pseudoClassStateChanged(errorClass, false);
            } catch (NumberFormatException numex) {
                phaseUnitsTextField.pseudoClassStateChanged(errorClass, true);
                phase.units = 0;
                // leave text displayed
            }
        } else {
            phaseUnitsTextField.setText("");
        }
    }


    @FXML
    private void setPhaseDriverStart() {
        Optional<Phase> optionalPhase = getCurrentPhase();
        if (optionalPhase.isPresent()) {
            Phase phase = optionalPhase.get();
            phase.driverTF.setStart(driverStartDatePicker.getValue());
        } else {
            driverStartDatePicker.setValue(null);
        }
    }

    @FXML
    private void setPhaseDriversEnd() {
        Optional<Phase> optionalPhase = getCurrentPhase();
        if (optionalPhase.isPresent()) {
            Phase phase = optionalPhase.get();
            phase.driverTF.setEnd(driverEndDatePicker.getValue());
        } else {
            driverEndDatePicker.setValue(null);
        }

    }


    @FXML
    private void setNotes() {
        Optional<Phase> optionalPhase = getCurrentPhase();
        if (optionalPhase.isPresent()) {
            optionalPhase.get().notes = notesTextArea.getText();;
        } else {
            notesTextArea.setText("");
        }
    }

    public static String getCurrentFQN() {
        // 9 character job name
        // 3 digit phase max
        // 3 digit unit

        var ref = new Object() {
            String retval;
        };

        getCurrentJob().ifPresent(job -> {
            if (job.name.length() > 9) {
                ref.retval = job.name.substring(0, 9);
            } else {
                ref.retval = job.name;
            }
        });

        getCurrentPhase().ifPresent(phase -> {
            if (phase.name.length() > 3) {
                ref.retval += ":" + phase.name.substring(0, 4);
            } else {
                ref.retval += ":" + phase.name;
            }

            if (phase.units > 999) {
                ref.retval += ":999"; // three digit max
            } else {
                ref.retval += ":" + phase.units;
            }
        });

        return ref.retval;
    }

    private String getDateString(LocalDate localDate) {
        if (localDate == null) return "---";
        int day = localDate.getDayOfMonth();
        int month = localDate.getMonth().getValue();
        int year = localDate.getYear();
        return (month + "/" + day + "/" + year);
    }

}
