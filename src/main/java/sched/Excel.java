package sched;

import org.apache.poi.ooxml.POIXMLProperties;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONStringer;
import org.json.JSONObject;
import org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperty;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.*;

public class Excel {

    private static int VIEW_WEEK_COLUMNS;
    public static final int FOREMAN_START_COLUMN = 2;
    public static final int FOREMAN_BLOCK_ROW_COUNT = 10;
    // G123 = <TYPE><NUMBER> (4 digits) i.e. GRADAL TRUCK 123
    public static final ArrayList<LocalDate> weekEndings = new ArrayList<>();

    public static final XSSFWorkbook workbook = new XSSFWorkbook();  // TODO: might have to pass OPC package
    public static final XSSFSheet foresightSheet = workbook.createSheet("Foresight");
    public static final XSSFSheet legendSheet = workbook.createSheet("Legend");
    public static final CellStyle dayOfWeekCellStyle = getDayOfWeekCellStyle();
    public static final CellStyle driverStatusCellStyle = getDriverStatusCellStyle();
    public static final CellStyle assistantCellStyle = getAssistantCellStyle();
    public static final CellStyle driverCellStyle = getDriverCellStyle();
    public static final CellStyle offWorkCellStyle = getOffWorkCellStyle();
    public static final CellStyle vacationCellStyle = getVacationCellStyle();
    public static final CellStyle terminationCellStyle = getTerminationCellStyle();
    public static final CellStyle regularDayCellStyle = getRegularDayCellStyle();


    public static CellStyle getDayOfWeekCellStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Consolas");
        font.setBold(true);
        cellStyle.setFont(font);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setRotation((short)-90);
        return cellStyle;
    }

    public static CellStyle getDriverStatusCellStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.SEA_GREEN.index);
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        return cellStyle;
    }

    public static CellStyle getDriverCellStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.SEA_GREEN.index);
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBorderBottom(BorderStyle.DOTTED);
        cellStyle.setBorderTop(BorderStyle.DOTTED);
        cellStyle.setBorderLeft(BorderStyle.DOTTED);
        cellStyle.setBorderRight(BorderStyle.DOTTED);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        return cellStyle;
    }

    public static CellStyle getAssistantCellStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.index);
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setAlignment(HorizontalAlignment.LEFT);
        return cellStyle;
    }

    public static CellStyle getOffWorkCellStyle() {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFillPattern(FillPatternType.SPARSE_DOTS);
        cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
        return cellStyle;
    }

    public static CellStyle getVacationCellStyle() {
        Font vacationFont = Excel.workbook.createFont();
        vacationFont.setFontHeightInPoints((short)10);
        vacationFont.setBold(true);
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(vacationFont);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setFillForegroundColor(IndexedColors.ROYAL_BLUE.index);
        cellStyle.setFillPattern(FillPatternType.THIN_FORWARD_DIAG);
        return cellStyle;
    }


    public static CellStyle getTerminationCellStyle() {
        Font terminationFont = Excel.workbook.createFont();
        terminationFont.setFontHeightInPoints((short)10);
        terminationFont.setBold(true);
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(terminationFont);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setFillForegroundColor(IndexedColors.DARK_RED.index);
        cellStyle.setFillPattern(FillPatternType.DIAMONDS);
        return cellStyle;
    }

    public static CellStyle getRegularDayCellStyle() {
        Font regularFont = Excel.workbook.createFont();
        regularFont.setFontHeightInPoints((short)10);
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(regularFont);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        return cellStyle;
    }


    /**
     _________                        __           ___________                   .__
     \_   ___ \_______   ____ _____ _/  |_  ____   \_   _____/__  ___ ____  ____ |  |
     /    \  \/\_  __ \_/ __ \\__  \\   __\/ __ \   |    __)_\  \/  // ___\/ __ \|  |
     \     \____|  | \/\  ___/ / __ \|  | \  ___/   |        \>    <\  \__\  ___/|  |__
      \______  /|__|    \___  >____  /__|  \___  > /_______  /__/\_ \\___  >___  >____/
             \/             \/     \/          \/          \/      \/    \/    \/
     */
    public static void create() {
        VIEW_WEEK_COLUMNS = PrimaryController.viewWeeks;
        writeHeader(PrimaryController.viewStart.get()); // row 1
        PrimaryController.progress.setValue(0.1);

        Foresight.foremen.sort(new ForemanComparator());

        int amountOfForemen = Foresight.foremen.size();

        int rowCount = 1;  // row 2
        for (Foreman foreman: Foresight.foremen) {

            // for every foreman add a status bar
            rowCount = writeStatusBar(foreman, rowCount);
            writeForemanInfo(foreman, rowCount);
            writeForemanBlock(foreman, rowCount);
            writeForemanNotes(foreman, rowCount);
            rowCount += 10;
            PrimaryController.progress.setValue(amountOfForemen/(amountOfForemen + 12));
        }
        foresightSheet.autoSizeColumn(0);
        foresightSheet.setColumnWidth(1, 500);  // day of the week cells //1100
//        workbook.setPrintArea(0, 0,FOREMAN_START_COLUMN + VIEW_WEEK_COLUMNS + 2,
//                              0, rowCount);
        PrimaryController.progress.setValue(0.95);
        writeLegend();
        embedJson();
    }


    private static void writeHeader(LocalDate viewStart) {

        CellStyle headerCellStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Consolas");
        font.setBold(true);
        headerCellStyle.setFont(font);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
        headerCellStyle.setBorderBottom(BorderStyle.DOTTED);

        LocalDate weekend = viewStart;

        XSSFRow row = foresightSheet.createRow(0);
        CellStyle borderCellStyle = workbook.createCellStyle();
        borderCellStyle.setFillPattern(FillPatternType.DIAMONDS);

        row.createCell(0).setCellStyle(borderCellStyle);
        row.createCell(1).setCellStyle(borderCellStyle);

        weekEndings.clear(); // in case of prior
        int column;
        for (column = FOREMAN_START_COLUMN; column < (FOREMAN_START_COLUMN + VIEW_WEEK_COLUMNS); column++) {
            weekEndings.add(weekend);
            XSSFCell cell = row.createCell(column);
            cell.setCellValue(weekend.getDayOfMonth()
                              + "-"
                              + weekend.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH)
                              + "-"
                              + weekend.getYear());
            cell.setCellStyle(headerCellStyle);
            foresightSheet.setColumnWidth(column, 3900);
            weekend = weekend.plusDays(7);
        }

        XSSFCell liftTruckCell = row.createCell(column);
        liftTruckCell.setCellValue("LIFT");

        Font liftHeaderFont = workbook.createFont();
        liftHeaderFont.setFontName("Consolas");
        liftHeaderFont.setBold(true);
        CellStyle liftHeaderCellStyle = workbook.createCellStyle();
        liftHeaderCellStyle.setAlignment(HorizontalAlignment.CENTER);
        liftHeaderCellStyle.setBorderRight(BorderStyle.THIN);
        liftHeaderCellStyle.setBorderLeft(BorderStyle.THIN);
        liftHeaderCellStyle.setBorderTop(BorderStyle.THIN);
        liftHeaderCellStyle.setBorderBottom(BorderStyle.THIN);
        liftHeaderCellStyle.setFont(liftHeaderFont);

        liftTruckCell.setCellStyle(liftHeaderCellStyle);

//        foresightSheet.setDefaultColumnStyle(column, liftHeaderCellStyle); // will be overwritten
        foresightSheet.setColumnWidth(column, 2040);
        foresightSheet.createFreezePane(2, 1); // relative freeze, row 0 and col 0 are frozen
    }


    public static int writeStatusBar(Foreman foreman, int row) {
        XSSFRow driverStatusRow = foresightSheet.createRow(row++);
        driverStatusRow.setRowStyle(null);
        driverStatusRow.setHeightInPoints(4);
        CellStyle borderCellStyle = workbook.createCellStyle();
        borderCellStyle.setFillPattern(FillPatternType.DIAMONDS);

        boolean paintWeek = false;
        for (int weekColumn = FOREMAN_START_COLUMN; weekColumn < (FOREMAN_START_COLUMN + VIEW_WEEK_COLUMNS); weekColumn++) {
            for (Job job: foreman.jobs) {
                for (Phase phase: job.phases) {
                    if (phase.driverIsWorking(weekEndings.get(weekColumn - 2))) {
                        paintWeek = true;
                        break;
                    }
                }
                if (paintWeek) break;
            }

            if (paintWeek) {
                driverStatusRow.createCell(weekColumn).setCellStyle(driverStatusCellStyle);
                paintWeek = false; // to check the next week
            } else {
                driverStatusRow.createCell(weekColumn).setCellStyle(borderCellStyle);
            }
        }

        // Paint the cells
        driverStatusRow.createCell(0).setCellStyle(borderCellStyle);
        driverStatusRow.createCell(1).setCellStyle(borderCellStyle);

        driverStatusRow.createCell(FOREMAN_START_COLUMN + VIEW_WEEK_COLUMNS).setCellStyle(borderCellStyle);
        driverStatusRow.createCell(FOREMAN_START_COLUMN + VIEW_WEEK_COLUMNS + 1).setCellStyle(borderCellStyle);

        return row;
    }

    public static void writeForemanBlock(Foreman foreman, int rowCount) {

        for (int weekColumn = FOREMAN_START_COLUMN; weekColumn < (FOREMAN_START_COLUMN + VIEW_WEEK_COLUMNS); weekColumn++) {

            int offset = 4;

            for (int dayRow = rowCount; dayRow < (rowCount + FOREMAN_BLOCK_ROW_COUNT); dayRow += 2) { // two rows a day

                LocalDate today = weekEndings.get(weekColumn - FOREMAN_START_COLUMN).minusDays(offset--);
                XSSFCell topCell = foresightSheet.getRow(dayRow).createCell(weekColumn);
                XSSFCell bottomCell = foresightSheet.getRow(dayRow + 1).createCell(weekColumn);

                if (foreman.isWorking(today)) {
                    topCell.setCellStyle(regularDayCellStyle);
                    bottomCell.setCellStyle(regularDayCellStyle);

                    // retrieve Action
                    Action todaysAction = foreman.viewMap.get(today);
                    if (todaysAction != null) {
                        // there is an action today!
                        topCell.setCellValue(todaysAction.fqn);
                        if (todaysAction.type == Action.Type.LUMBER) {
                            bottomCell.setCellValue(todaysAction.getExcelName() + ":" + todaysAction.lumberDriver);
                        } else if (todaysAction.type == Action.Type.JOB_COMPLETE) {
                            bottomCell.setCellValue("COMPLETE");
                        } else {
                            bottomCell.setCellValue(todaysAction.getExcelName() + ":" + todaysAction.actioneer);
                        }
                        bottomCell.setCellStyle(todaysAction.style);
                    }

                } else {
                    topCell.setCellStyle(offWorkCellStyle);
                    bottomCell.setCellStyle(offWorkCellStyle);
                }

                // vacation highlighting will override active
                // TODO: make sure it doesn't override action
                if (foreman.onVacation(today)) {
                    topCell.setCellStyle(vacationCellStyle);
                    bottomCell.setCellStyle(vacationCellStyle);
                }

                // termination highlight **highest priority**
                if (foreman.termination != null && today.isEqual(foreman.termination)) {
                    topCell.setCellStyle(terminationCellStyle);
                    bottomCell.setCellStyle(terminationCellStyle);
                    // Allow to see if assigned things after termination
                }

            }
        }

    }


    public static void writeForemanInfo(Foreman foreman, int startRow) {
        HashSet<String> currentDrivers = new HashSet<>(); // to prevent repeats
        for (Job job: foreman.jobs) {
            for (Driver driver: job.drivers) {
                currentDrivers.add(driver.name);
            }
        }

        Iterator<String> iter = currentDrivers.iterator();
        // TODO: only shows up to 5 drivers
        int amountOfDrivers = currentDrivers.size();

        for (int i = startRow; i < startRow + 10; i++) {
            XSSFRow r = foresightSheet.createRow(i);
            r.setRowStyle(regularDayCellStyle);
            XSSFCell foremanInfoCell = r.createCell(0); // always first column
            XSSFCell dayCell = r.createCell(1);
            if (i == startRow) {
                dayCell.setCellValue("MON");
                foremanInfoCell.setCellValue(foreman.name);
                foremanInfoCell.setCellStyle(foreman.getCellStyle(workbook));
            } else if (i == startRow + 1) {
                foremanInfoCell.setCellValue("Asst:" + foreman.assistantName);
                foremanInfoCell.setCellStyle(assistantCellStyle);
            } else if (i == startRow + 2) {
                dayCell.setCellValue("TUE");
                foremanInfoCell.setCellValue(">>Drivers<<");
                CellStyle style = workbook.createCellStyle();
                style.setAlignment(HorizontalAlignment.CENTER);
                foremanInfoCell.setCellStyle(style);
            } else if (i == startRow + 3) {
                if (amountOfDrivers > 0) {
                    if (iter.hasNext()) foremanInfoCell.setCellValue(iter.next());
                    foremanInfoCell.setCellStyle(driverCellStyle);
                }
            } else if (i == startRow + 4) {
                dayCell.setCellValue("WED");
                if (amountOfDrivers > 1) {
                    if (iter.hasNext()) foremanInfoCell.setCellValue(iter.next());
                    foremanInfoCell.setCellStyle(driverCellStyle);
                }
            } else if (i == startRow + 5) {
                if (amountOfDrivers > 2) {
                    if (iter.hasNext()) foremanInfoCell.setCellValue(iter.next());
                    foremanInfoCell.setCellStyle(driverCellStyle);
                }
            } else if (i == startRow + 6) {
                dayCell.setCellValue("THU");
                if (amountOfDrivers > 3) {
                    if (iter.hasNext()) foremanInfoCell.setCellValue(iter.next());
                    foremanInfoCell.setCellStyle(driverCellStyle);
                }
            } else if (i == startRow + 7) {
                if (amountOfDrivers > 4) {
                    if (iter.hasNext()) foremanInfoCell.setCellValue(iter.next());
                    foremanInfoCell.setCellStyle(driverCellStyle);
                }
            } else if (i == startRow + 8) {
                dayCell.setCellValue("FRI");
            }
            dayCell.setCellStyle(dayOfWeekCellStyle);
        }

        // Merge the cells for cleanliness
        for (int m = startRow; m < startRow + 10; m+=2) {
            CellRangeAddress region = new CellRangeAddress(m, m + 1, 1, 1);
            try {
                foresightSheet.addMergedRegion(region);
            } catch (IllegalStateException ex) {
                // silently fail
            }
            if (m <= startRow + 6) RegionUtil.setBorderBottom(BorderStyle.THICK, region, foresightSheet);
        }
    }


    public static void writeForemanNotes(Foreman foreman, int rowCount) {

        final int LIFT_TRUCK_COLUMN = FOREMAN_START_COLUMN + VIEW_WEEK_COLUMNS;
        final int FOREMAN_NOTES_COLUMN = FOREMAN_START_COLUMN + VIEW_WEEK_COLUMNS + 1;

        // build out notes column
        ArrayList<LiftTruck> currentTrucks = new ArrayList<>();  // and LIFT_TRUCKS (to save an iteration)
        ArrayList<Notes> foremanNotes = new ArrayList<>();


        // POSITION:JOB=NAME
        for (Job job: foreman.jobs) {
            foremanNotes.add(new Notes(job));
            currentTrucks.addAll(job.liftTrucks);
        }

        ///  L I F T    T R U C K S   C O L U M N
        int truckNum = 0;
        int truckRow = rowCount;
        for (;(truckNum < currentTrucks.size()) && (truckNum < FOREMAN_BLOCK_ROW_COUNT); truckRow++, truckNum++) {

            XSSFCell liftCell = foresightSheet.getRow(truckRow).createCell(LIFT_TRUCK_COLUMN);
            LiftTruck liftTruck = currentTrucks.get(truckNum);
            liftCell.setCellStyle(liftTruck.truckType.getCellStyle()); // redundant but necessary
            liftCell.setCellValue(liftTruck.getExcelString());
        }

        // Fill in the rest of the TRUCK COLUMN
        //        foresightSheet.setDefaultColumnStyle(column, liftHeaderCellStyle); // will be overwritten
        for (; truckRow < (rowCount + FOREMAN_BLOCK_ROW_COUNT); truckRow++) {
            XSSFCell liftCell = foresightSheet.getRow(truckRow).createCell(LIFT_TRUCK_COLUMN);
            liftCell.setCellStyle(regularDayCellStyle);
        }


        ///  N O T E S   C O L U M N
        int writtenCount = 0;
        for (Notes notes: foremanNotes) {


            if (!notes.developer.equals("") && (writtenCount < rowCount + FOREMAN_BLOCK_ROW_COUNT)) {
                XSSFCell notesCell = foresightSheet.getRow(rowCount + writtenCount++).createCell(FOREMAN_NOTES_COLUMN);
                notesCell.setCellValue(notes.developer);
                notesCell.setCellStyle(regularDayCellStyle);
            }

            if (!notes.superintendent.equals("") && (writtenCount < rowCount + FOREMAN_BLOCK_ROW_COUNT)) {
                XSSFCell notesCell = foresightSheet.getRow(rowCount + writtenCount++).createCell(FOREMAN_NOTES_COLUMN);
                notesCell.setCellValue(notes.superintendent);
                notesCell.setCellStyle(regularDayCellStyle);
            }

            if (!notes.lumber.equals("") && (writtenCount < rowCount + FOREMAN_BLOCK_ROW_COUNT)) {
                XSSFCell notesCell = foresightSheet.getRow(rowCount + writtenCount++).createCell(FOREMAN_NOTES_COLUMN);
                notesCell.setCellValue(notes.lumber);
                notesCell.setCellStyle(regularDayCellStyle);
            }

        }

        foresightSheet.setColumnWidth(FOREMAN_NOTES_COLUMN, 7000); // 33 chars

    }


    public static void embedJson() {

        JSONStringer stringer = new JSONStringer();
        stringer.object();
        stringer.key("author");
        stringer.value(PrimaryController.author.get());
        stringer.key("viewStart");
        stringer.value(PrimaryController.viewStart.get().toString());
        stringer.key("viewWeeks");
        stringer.value(VIEW_WEEK_COLUMNS);
        stringer.key("foremen");
        stringer.array();

        for (Foreman foreman: Foresight.foremen) {
            stringer.object();
            stringer.key("name");
            stringer.value(foreman.name);
            stringer.key("county");
            stringer.value(foreman.county.toString());
            stringer.key("assistant");
            stringer.value(foreman.assistantName);
            stringer.key("termination");
            stringer.value(foreman.termination == null ? "" : foreman.termination.toString());

            stringer.key("vacations");
            stringer.array();
            for (Vacation vacation: foreman.vacations) {
                stringer.object();
                stringer.key("name");
                stringer.value(vacation.name);
                stringer.key("tfStart");
                stringer.value(vacation.tf.getStart() == null ? "" : vacation.tf.getStart().toString());
                stringer.key("tfEnd");
                stringer.value(vacation.tf.getEnd() == null ? "" : vacation.tf.getEnd().toString());
                stringer.endObject();
            }
            stringer.endArray();

            stringer.key("jobs");
            stringer.array();
            for (Job job: foreman.jobs) {
                stringer.object();
                stringer.key("name");
                stringer.value(job.name);
                stringer.key("developer");
                stringer.value(job.developer);
                stringer.key("superintendent");
                stringer.value(job.superintendent);
                stringer.key("lumber");
                stringer.value(job.lumber);
                stringer.key("notes");
                stringer.value(job.notes);

                stringer.key("drivers");
                stringer.array();
                for (Driver driver: job.drivers) {
                    stringer.object();
                    stringer.key("name");
                    stringer.value(driver.name);
                    stringer.endObject();
                }
                stringer.endArray(); // end driver

                stringer.key("liftTrucks");
                stringer.array();
                for (LiftTruck liftTruck: job.liftTrucks) {
                    stringer.object();
                    stringer.key("name");
                    stringer.value(liftTruck.name);
                    stringer.key("type");
                    stringer.value(liftTruck.truckType.toString());
                    stringer.key("size");
                    stringer.value(liftTruck.truckSize.toString());
                    stringer.endObject();
                }
                stringer.endArray(); // end liftTrucks


                stringer.key("phases");
                stringer.array();
                for (Phase phase: job.phases) {
                    stringer.object();
                    stringer.key("name");
                    stringer.value(phase.name);
                    stringer.key("driverStart");
                    stringer.value(phase.driverTF.getStart() == null ? "" : phase.driverTF.getStart().toString());
                    stringer.key("driverEnd");
                    stringer.value(phase.driverTF.getEnd() == null ? "" : phase.driverTF.getEnd().toString());
                    stringer.key("units");
                    stringer.value(phase.units);
                    stringer.key("notes");
                    stringer.value(phase.notes);
                    stringer.key("tfStart");
                    stringer.value(phase.tf.getStart() == null ? "" : phase.tf.getStart().toString());
                    stringer.key("tfEnd");
                    stringer.value(phase.tf.getEnd() == null ? "" : phase.tf.getEnd().toString());

                    stringer.key("actions");
                    stringer.array();
                    for (Action action: phase.actions) {
                        stringer.object();
                        stringer.key("number");
                        stringer.value(action.number);
                        stringer.key("type");
                        stringer.value(action.type.toString());
                        stringer.key("actioneer");
                        stringer.value(action.actioneer);
                        stringer.key("lumberDriver");
                        stringer.value(action.lumberDriver);
                        stringer.key("day");
                        stringer.value(action.day == null ? "" : action.day.toString());
                        stringer.key("notes");
                        stringer.value(action.notes);
                        stringer.key("fqn");
                        stringer.value(action.fqn);
                        stringer.endObject(); // end action object
                    }
                    stringer.endArray(); // end action array


                    stringer.endObject(); // end phase object
                }
                stringer.endArray(); // end phase array

                stringer.endObject(); // end job
            }
            stringer.endArray();  // end job array

            stringer.endObject(); // end foreman object
        }
        stringer.endArray(); // end foreman array

        stringer.key("knownDrivers");
        stringer.array();
        for (Driver driver: Foresight.knownDrivers) {
            stringer.object();
            stringer.key("name");
            stringer.value(driver.name); // just names not timeframes
            stringer.endObject();
        }
        stringer.endArray(); // end known Drivers array

        stringer.endObject(); // main object

        POIXMLProperties workbookProperties = workbook.getProperties();
        POIXMLProperties.CustomProperties custProps = workbookProperties.getCustomProperties();
        CTProperty property = custProps.getProperty("data");
        if (property == null) {
            custProps.addProperty("data", stringer.toString());
        } else {
            // TODO: there are other properties with *strings
            // TODO: if needed, break up json data into separate strings
            property.setLpwstr(stringer.toString());
        }
//        File file = new File("C:\\Users\\tony\\Desktop\\viewmap.json");
//        try {
//            if (file.createNewFile()) {
//                System.out.println("File created: " + file.getName());
//            } else {
//                System.out.println("File already exists.");
//            }
//            JSONObject jsonObject = new JSONObject(stringer.toString());
//            FileWriter myWriter = new FileWriter(file);
//            myWriter.write(jsonObject.toString(4));
//            myWriter.close();
//        } catch (IOException e) {
//            System.out.println("An error occurred.");
//            e.printStackTrace();
//        }
    }


    public static void writeLegend() {
        int rowCount = 1; // to leave a border...
        int keyCol = 1;
        int valCol = 2;
        Cell cell; // placeholder to write out data

        legendSheet.setColumnWidth(1, 2500);
        legendSheet.setColumnWidth(2, 16000);
        XSSFRow row = legendSheet.createRow(rowCount++);
        row.setHeightInPoints(36);

        Font font = workbook.createFont();
        font.setFontHeightInPoints((short)36);
        font.setFontName("Old English Text MT");

        CellStyle legendStyle = workbook.createCellStyle();
        legendStyle.setFont(font);

        cell = row.createCell(1);
        cell.setCellStyle(legendStyle);
        cell.setCellValue("Foresight Legend");


        CellRangeAddress region = new CellRangeAddress(1, 1, 1, 2);
        try {
            legendSheet.addMergedRegion(region);
        } catch (IllegalStateException ex) {
            // silently fail
        }
        RegionUtil.setBorderBottom(BorderStyle.THICK, region, legendSheet);

        XSSFRow driverRow = legendSheet.createRow(rowCount++);
        cell = driverRow.createCell(keyCol);
        driverRow.setHeightInPoints(20);
        cell.setCellStyle(driverCellStyle);
        cell = driverRow.createCell(valCol);
        cell.setCellValue("Driver StatusBar highlighted cell indicates driver working that week");

        XSSFRow assistantRow = legendSheet.createRow(rowCount++);
        assistantRow.setHeightInPoints(20);
        cell = assistantRow.createCell(keyCol);
        cell.setCellStyle(assistantCellStyle);
        cell = assistantRow.createCell(valCol);
        cell.setCellValue("Assistant Foreman");

        XSSFRow offWorkRow = legendSheet.createRow(rowCount++);
        offWorkRow.setHeightInPoints(20);
        cell = offWorkRow.createCell(keyCol);
        cell.setCellStyle(offWorkCellStyle);
        cell = offWorkRow.createCell(valCol);
        cell.setCellValue("No more work is scheduled for relative Foreman");

        XSSFRow vacationRow = legendSheet.createRow(rowCount++);
        vacationRow.setHeightInPoints(20);
        cell = vacationRow.createCell(keyCol);
        cell.setCellStyle(vacationCellStyle);
        cell = vacationRow.createCell(valCol);
        cell.setCellValue("Vacation Cells, NOTE: Actions will still show in vacation cells if assigned");

        XSSFRow terminationRow = legendSheet.createRow(rowCount++);
        terminationRow.setHeightInPoints(20);
        cell = terminationRow.createCell(keyCol);
        cell.setCellStyle(terminationCellStyle);
        cell = terminationRow.createCell(valCol);
        cell.setCellValue("Termination Cell, Foreman no longer works for company after this date");

        // actions
        XSSFRow trenchRow = legendSheet.createRow(rowCount++);
        trenchRow.setHeightInPoints(20);
        cell = trenchRow.createCell(keyCol);
        cell.setCellStyle(Action.trenchStyle);
        cell = trenchRow.createCell(valCol);
        cell.setCellValue("Trench Action");

        XSSFRow slabRow = legendSheet.createRow(rowCount++);
        slabRow.setHeightInPoints(20);
        cell = slabRow.createCell(keyCol);
        cell.setCellStyle(Action.slabStyle);
        cell = slabRow.createCell(valCol);
        cell.setCellValue("Slab Action");

        XSSFRow lumberRow = legendSheet.createRow(rowCount++);
        lumberRow.setHeightInPoints(20);
        cell = lumberRow.createCell(keyCol);
        cell.setCellStyle(Action.lumberStyle);
        cell = lumberRow.createCell(valCol);
        cell.setCellValue("Lumber Action");

        XSSFRow snapRow = legendSheet.createRow(rowCount++);
        snapRow.setHeightInPoints(20);
        cell = snapRow.createCell(keyCol);
        cell.setCellStyle(Action.snapStyle);
        cell = snapRow.createCell(valCol);
        cell.setCellValue("Snap Action");

        XSSFRow layoutRow = legendSheet.createRow(rowCount++);
        layoutRow.setHeightInPoints(20);
        cell = layoutRow.createCell(keyCol);
        cell.setCellStyle(Action.layoutStyle);
        cell = layoutRow.createCell(valCol);
        cell.setCellValue("Layout Action");

        XSSFRow roofsheetRow = legendSheet.createRow(rowCount++);
        roofsheetRow.setHeightInPoints(20);
        cell = roofsheetRow.createCell(keyCol);
        cell.setCellStyle(Action.roofsheetStyle);
        cell = roofsheetRow.createCell(valCol);
        cell.setCellValue("Roofsheet Action");

        XSSFRow jobCompleteRow = legendSheet.createRow(rowCount++);
        jobCompleteRow.setHeightInPoints(20);
        cell = jobCompleteRow.createCell(keyCol);
        cell.setCellValue("COMPLETE");
        cell.setCellStyle(Action.completeStyle);
        cell = jobCompleteRow.createCell(valCol);
        cell.setCellValue("Job Complete Action, for related Phase");



        // lift trucks
        XSSFRow rentalRow = legendSheet.createRow(rowCount++);
        cell = rentalRow.createCell(keyCol);
        cell.setCellStyle(LiftTruck.TruckType.RENTAL.getCellStyle());
        cell = rentalRow.createCell(valCol);
        cell.setCellValue("Rental Lift Truck");

        XSSFRow jlgRow = legendSheet.createRow(rowCount++);
        cell = jlgRow.createCell(keyCol);
        cell.setCellStyle(LiftTruck.TruckType.JLG.getCellStyle());
        cell = jlgRow.createCell(valCol);
        cell.setCellValue("JLG Lift Truck");

        XSSFRow manatuRow = legendSheet.createRow(rowCount);
        cell = manatuRow.createCell(keyCol);
        cell.setCellStyle(LiftTruck.TruckType.MANATU.getCellStyle());
        cell = manatuRow.createCell(valCol);
        cell.setCellValue("Manatu Lift Truck");

    }

}

