package sched;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PrimaryController {

    @FXML TextField authorTextField;
    @FXML TextField savePathTextField;
    @FXML DatePicker viewStartDatePicker;
    @FXML TextField viewWeeksTextField;
    @FXML ProgressBar progressBar;

    public static SimpleStringProperty author;
    public static SimpleObjectProperty<File> saveFile;
    public static SimpleObjectProperty<LocalDate> viewStart;
    public static int viewWeeks;
    public static SimpleIntegerProperty progress;
    final PseudoClass errorClass = PseudoClass.getPseudoClass("error");

    public static int numOfWeeks;

//  Foresight.setRoot("secondary");

    public void initialize() {
        String userHome = System.getProperty("user.home");
        Path defaultPath = FileSystems.getDefault().getPath(userHome, "Desktop", "foresight.xlsx");
        savePathTextField.setPromptText(IO.getRelativePath(defaultPath.toFile()));
        savePathTextField.setEditable(false);
        author = new SimpleStringProperty();
        saveFile = new SimpleObjectProperty<>();
        viewStart = new SimpleObjectProperty<>();
        progress = new SimpleIntegerProperty();
        viewWeeks = 12; // default to 12 weeks
        author.set("Circle M Contractors");

        saveFile.set(defaultPath.toFile());
        saveFile.addListener((obsv, oldValue, newValue) -> {
            Pattern pattern = Pattern.compile(".*([\\\\/].*?[\\\\/].*)");
            Matcher matcher = pattern.matcher(newValue.getPath());
            if (matcher.matches()) {
                savePathTextField.setText("..." + matcher.group(1));
            } else {
                savePathTextField.setText("..." + File.pathSeparator + newValue.getName());
            }
        });

        viewStart.set(getDefaultViewStart()); // this will be overwritten by parse
        viewStart.addListener((obsv, oldValue, newValue) -> {
            viewStartDatePicker.setValue(newValue);
        });

        progress.addListener((obsv, oldValue, newValue) -> {
            progressBar.setProgress(newValue.floatValue());
        });

        Parser.loaded.addListener((observableValue, oldValue, newValue) -> {
            authorTextField.setText(author.get());
            viewStartDatePicker.setValue(viewStart.getValue());
            viewWeeksTextField.setText(String.valueOf(viewWeeks));
        });

    }

    @FXML
    private void handleMenuOpen() {
        IO.openFile();
    }

    @FXML
    private void handleMenuExit() {
        Foresight.close();
    }

    @FXML
    private void generateXlsx() {
        IO.generateXlsx();
    }

    @FXML
    private void setAuthor() {
        author.set(authorTextField.getText());
    }

    @FXML
    private void setViewWeeks() {
        String weeksStr = viewWeeksTextField.getText();
        try {
            viewWeeks = weeksStr.equals("") ? 12 : Integer.parseInt(weeksStr);
            viewWeeksTextField.pseudoClassStateChanged(errorClass, false);
        } catch (NumberFormatException ex) {
            viewWeeksTextField.pseudoClassStateChanged(errorClass, true);
            viewWeeks = 12;
            // leave text displayed
        }
    }

    @FXML
    private void setSavePath() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel", "*.xlsx")
        );

        File selected_file;
        String userHome = System.getProperty("user.home");

        Path lastDirPath = FileSystems.getDefault().getPath(userHome, ".foresight");
        try {

            if (Files.exists(lastDirPath)) {
                String remembered = Files.readString(lastDirPath);
                if (Files.exists(FileSystems.getDefault().getPath(remembered))) {
                    fileChooser.setInitialDirectory(new File(remembered).getParentFile());
                } else {
                    fileChooser.setInitialDirectory(new File(userHome));
                }

            } else {
                fileChooser.setInitialDirectory(new File(userHome));
            }

            selected_file = fileChooser.showSaveDialog(Foresight.this_stage);
            if (selected_file != null) {
                Files.writeString(lastDirPath, selected_file.getAbsolutePath()); // remember previous location
                PrimaryController.saveFile.set(selected_file);
                savePathTextField.setText(IO.getRelativePath(selected_file));
            } else {
                String author = authorTextField.getText();
                if (author.equals("")) {
                    Path defaultPath = FileSystems.getDefault().getPath(userHome, "Desktop", "foresight.xlsx");
                    // need to add logic
                    savePathTextField.setText(IO.getRelativePath(defaultPath.toFile()));
                } else {
                    Path authorPath = FileSystems.getDefault().getPath(userHome, "Desktop", author + "-foresight.xlsx");
                    savePathTextField.setText(IO.getRelativePath(authorPath.toFile()));
                }
            }
            // else user canceled operation

        } catch (IOException ex) {
            System.out.printf("setSavePath exception.... %s\n", ex.getMessage());
        }

    }

    public static LocalDate getDefaultViewStart() {
        //set to this friday
        LocalDate today = LocalDate.now();
        while (!today.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH).equals("Friday")) {
            today = today.plusDays(1);
        }
        return today;
    }

    @FXML
    private void setViewStart() {
        LocalDate givenDay = viewStartDatePicker.getValue();
        while (!givenDay.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH).equals("Friday")) {
            givenDay = givenDay.plusDays(1);
        }
        viewStart.set(givenDay);
    }

    @FXML
    private void handleAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About Foresight");
        alert.setHeaderText("Look Ahead with Foresight");
        alert.setContentText("Made with love from Tony B\nThis was a proof of concept that turned into something more.\n" +
                "So save some time and get back to surfing!");
        alert.showAndWait().ifPresent(rs -> {
            if (rs == ButtonType.OK) {
//                System.out.println("Pressed OK.");
            }
        });

    }

}
