package sched;

import javafx.application.Platform;
import javafx.stage.FileChooser;
import org.apache.poi.ooxml.POIXMLProperties;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperty;

import java.awt.*;
import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class IO {

    public static String getRelativePath(File file) {
        Pattern pattern = Pattern.compile(".*([\\\\/].*?[\\\\/].*)");
        Matcher matcher = pattern.matcher(file.getPath());
        if (matcher.matches()) {
            // with parent dir
            return "..." + matcher.group(1);
        } else {
            // without parent dir
            return "..." + File.pathSeparator + file.getName();
        }

    }

    public static void openFile() {

        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel", "*.xlsx")
        );

        File selected_file;
        String userHome = System.getProperty("user.home");

        Path lastDirPath = FileSystems.getDefault().getPath(userHome, ".foresight");
        try {

            if (Files.exists(lastDirPath)) {
                String remembered = Files.readString(lastDirPath);
                if (Files.exists(FileSystems.getDefault().getPath(remembered))) {
                    fileChooser.setInitialDirectory(new File(remembered).getParentFile());
                } else {
                    fileChooser.setInitialDirectory(new File(userHome));
                }

            } else {
                fileChooser.setInitialDirectory(new File(userHome));
            }

            selected_file = fileChooser.showOpenDialog(Foresight.this_stage);
            if (selected_file != null) {
                Files.writeString(lastDirPath, selected_file.getAbsolutePath()); // remember previous location
                loadFile(selected_file);
            }
            // else user canceled operation

        } catch (IOException ex) {
            Foresight.warnUserDialog("Couldn't open file", ex.getMessage());
        }
    }

    /**
     * This is to actually open the file with the operating system call
     * @param file
     */
    public static void nativeOpenFile(File file) {
        try {
            Desktop.getDesktop().open(file);
        } catch (IOException ex) {
            Logger.getLogger(Foresight.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void loadFile(File fileToLoad) {
        try {
            if (!Pattern.matches("^.*\\.xlsx$", fileToLoad.toString())) {
                Foresight.warnUserDialog("Unable to open file", "Incorrect file type supplied, only \".xlsx\" is supported");
                return;
            }
            // Getting the workbook instance for XLSX file
            FileInputStream _fip = new FileInputStream(fileToLoad);
            XSSFWorkbook workbook = new XSSFWorkbook(_fip);
            POIXMLProperties workbookProperties = workbook.getProperties();
            POIXMLProperties.CustomProperties custProps = workbookProperties.getCustomProperties();
            CTProperty data = custProps.getProperty("data");
            if (data != null) {
                String rawJson = data.getLpwstr();
                Parser.loadForesight(rawJson);
                PrimaryController.saveFile.set(fileToLoad);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Foresight.warnUserDialog("File not found", "Please check path supplied");
        } catch (IOException e) {
            e.printStackTrace();
            Foresight.warnUserDialog("Unable to read file", "Please check permissions");
        }

    }

    public static void generateXlsx() {

        Excel.create();

        POIXMLProperties xmlProps = Excel.workbook.getProperties();
        POIXMLProperties.CoreProperties coreProps =  xmlProps.getCoreProperties();
        // check for null string
        coreProps.setCreator(PrimaryController.author.get().equals("") ? "Circle M Contractors" : PrimaryController.author.get());

        try {
            String timestamp = new SimpleDateFormat("HH.mm.ss").format(new Date());
            File file = PrimaryController.saveFile.get();
            String[] nameArray = file.getName().split("_");
            if (nameArray.length > 1) {
                file = new File(file.getParent() + File.separator + timestamp + "_" + nameArray[1]);
            } else {
                file = new File(file.getParent() + File.separator + timestamp + "_" + file.getName());
            }
            FileOutputStream fileOut = new FileOutputStream(file);
            Excel.workbook.write(fileOut);
            PrimaryController.saveFile.set(file); // save for next round
            fileOut.close();

        } catch (Exception e) {
            Foresight.warnUserDialog("Foresight Warning", "Unable to create file "
                    + PrimaryController.saveFile.get().getAbsolutePath() + "\nPossible currently opened.");
            System.out.println(e.toString());
        }
        PrimaryController.progress.setValue(1.0); // finished!

        Platform.runLater(() -> {
            try {
                Thread.sleep(1500);
                PrimaryController.progress.setValue(0.0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

    }

}



// without timestamp, just increment
//
//            File file = PrimaryController.saveFile.get();
//            Pattern fileNamePattern = Pattern.compile(".*[\\\\/](.*)\\..\\w+");
//            Matcher fileNameMatcher = fileNamePattern.matcher(file.getPath());
//
//            if (file.exists()) {
//                //update save to reflect "new" file
//                if (fileNameMatcher.matches()) {
//                    // bump up the number
//                    String filename = fileNameMatcher.group(1);
//                    Pattern fileNumberPattern = Pattern.compile("(.*)(\\d+)");
//                    Matcher fileNumberMatcher = fileNumberPattern.matcher(filename);
//
//                    if (fileNumberMatcher.matches()) {
//                        String rootName = fileNumberMatcher.group(1);
//                        int num = Integer.parseInt(fileNumberMatcher.group(2));
//                        file = new File(file.getParent() +
//                                File.separator + rootName + (num + 1) + ".xlsx");
//                    } else {
//                        // no digit yet add one
//                        file = new File(file.getParent() +
//                                File.separator + file.getName().split("\\.")[0] + "1" + ".xlsx");
//                    }
//
//                } else {
//                    System.out.println("fileNameMatcher didn't work (shouldn't get here)");
//                }
//
//            } else {
//                file = new File(file.getParent() + File.separator +  file.getName());
//            }

