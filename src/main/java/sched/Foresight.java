package sched;

/*
 * Using 4 icons from https://fontawesome.com/license
 */

import java.util.ArrayList;
import java.io.IOException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

/**
 * JavaFX App
 */
public class Foresight extends Application {

    private static Scene scene;
    public static Stage this_stage; // for IO.java to access

    public static ArrayList<Foreman> foremen;      // root of the tree of data
    public static ArrayList<Driver>  knownDrivers; // list of known drivers (not necessarily assigned)

    public Foresight() {
        foremen = new ArrayList<>();
        knownDrivers = new ArrayList<>();
    }

    @Override
    public void start(Stage stage) throws IOException {
        this_stage = stage;

        scene = new Scene(loadFXML("primary"));
        scene.getStylesheets().add(getClass().getResource("text-field-error.css").toExternalForm());
        scene.getStylesheets().add(getClass().getResource("label-start.css").toExternalForm());
        scene.getStylesheets().add(getClass().getResource("label-complete.css").toExternalForm());
        scene.getStylesheets().add(getClass().getResource("list-style-action.css").toExternalForm());
        stage.setScene(scene);
        stage.setTitle("Foresight");
        stage.getIcons().add(new Image("file:src\\main\\resources\\sched\\geometry.png"));
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Foresight.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }


    /**
     * Closes GUI thread and app thread
     */
    public static void close() {
        Platform.exit();
        System.exit(0);
    }

    public static void warnUserDialog(String header, String warning) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Foresight Warning");
        alert.setHeaderText(header);
        alert.setContentText(warning);
        alert.showAndWait().ifPresent(rs -> {
            if (rs == ButtonType.OK) {
//                System.out.println("Pressed OK.");
            }
        });

    }

}