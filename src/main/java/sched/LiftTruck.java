package sched;

import org.apache.poi.ss.usermodel.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LiftTruck {
    public String name;
    public TruckType truckType;
    public TruckSize truckSize;


    public enum TruckType {
        RENTAL,
        JLG,
        MANATU,
        INVALID;

        public CellStyle getCellStyle() {
            Font font = Excel.workbook.createFont();
            font.setFontName("Consolas");
            font.setFontHeightInPoints((short)10);
            font.setBold(true);
            CellStyle cellStyle = Excel.workbook.createCellStyle();
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            cellStyle.setBorderLeft(BorderStyle.THIN);
            cellStyle.setBorderRight(BorderStyle.THIN);
            cellStyle.setFont(font);

            switch (this) {
                case RENTAL -> cellStyle.setFillForegroundColor(IndexedColors.GOLD.index);
                case JLG -> cellStyle.setFillForegroundColor(IndexedColors.PALE_BLUE.index);
                case MANATU -> cellStyle.setFillForegroundColor(IndexedColors.SEA_GREEN.index);
                default -> cellStyle.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.index);
            }
            return cellStyle;

        }
    }

    public enum TruckSize {
        SIZE_10,
        SIZE_12,
        INVALID
    }

    LiftTruck(int truckNum) {
        this.name = "New Lift Truck " + truckNum;
        this.truckType = TruckType.INVALID;
        this.truckSize = TruckSize.INVALID;
    }

    public String getExcelString() {
        // 7 chars with the -
        // <char><3digit#>-<size>

        // expected entry <Gradall 123>
        String outputString;

        Pattern pattern = Pattern.compile("([a-zA-Z]+).*?(\\d{1,3})");
        Matcher matcher = pattern.matcher(this.name);
        if (matcher.matches()) {
            String truckKind = matcher.group(1);
            String truckNumber = matcher.group(2);
            truckKind = truckKind.toUpperCase();
            char abbr = truckKind.charAt(0);
            outputString = abbr + truckNumber;
        } else {
            // TODO: shouldn't get here
            outputString = this.name; // set to raw truck string
        }

        switch (this.truckSize) {
            case SIZE_10 -> outputString += ("-" + "10");
            case SIZE_12 -> outputString += ("-" + "12");
        }
        return outputString;
    }

    public void setTruckType(String truckType) {
        switch (truckType) {
            case "RENTAL" -> this.truckType = TruckType.RENTAL;
            case "JLG" -> this.truckType = TruckType.JLG;
            case "MANATU" -> this.truckType = TruckType.MANATU;
            default -> this.truckType = TruckType.INVALID;
        }
    }

    public void setTruckSize(String truckSize) {
        switch (truckSize) {
            case "SIZE_10" -> this.truckSize = TruckSize.SIZE_10;
            case "SIZE_12" -> this.truckSize = TruckSize.SIZE_12;
            default -> this.truckSize = TruckSize.INVALID;
        }
    }

}
