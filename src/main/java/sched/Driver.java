package sched;


public class Driver {
    public String    name;
    public TimeFrame tf;

    /**
     * A Driver can have:
     * - more than one job
     * - more than one phase
     * - big job can have multiple drivers
     *
     * @param driverNum
     */
    Driver(int driverNum) {
        this.name = "New Driver " + driverNum;
        this.tf = new TimeFrame();
   }

    Driver(String name) {
        this.name = name;
        this.tf = new TimeFrame();
    }
}
