# Foresight
JavaFX GUI to generate schedules in Excel. Leverages the Apache POI library to generate Excel files. 

## Main features
1. Cascading selection, fields that are related are shown based on selection, thus reducing the scrolling needed 
to modify the schedule 
1. Consistent look and feel in the generated output, adding to readability of the documents
1. Persistent memory, Foresight will "remember" from previously generated files drastically reducing the overhead 
of modifying a single fields for newly generated files.
1. Custom viewable data, though Foresight will retain all data you can choose to _view_ only the part you wish to see. 

## Screenshots
![image](imgs/screenshot.png)

**Generated output** 
![image](imgs/excel_output.png)